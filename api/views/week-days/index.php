<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\WeekDaysSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Week Days';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="week-days-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Week Days', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'day_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

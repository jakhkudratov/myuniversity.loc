<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TeachScienceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Teach Sciences';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teach-science-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Teach Science', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'science_id',
            //'teacher_id',
            [
                'attribute'=> 'science_id',
                'value' => 'science.science_name',
                'format' => 'raw'
            ],
            [
                'attribute'=> 'teacher_id',
                'value' => 'teacher.full_name',
                'format' => 'raw'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

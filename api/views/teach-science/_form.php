<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TeachScience */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="teach-science-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= Html::dropDownList('science_id', 0, $sciences, ['class' => 'form-control']) ?>

    <?= Html::dropDownList('teacher_id', 0, $teachers, ['class' => 'form-control']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

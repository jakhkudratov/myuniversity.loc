<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TeachScience */

$this->title = 'Create Teach Science';
$this->params['breadcrumbs'][] = ['label' => 'Teach Sciences', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="teach-science-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'sciences' => $sciences,
        'teachers' => $teachers
    ]) ?>

</div>

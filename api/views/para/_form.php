<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Para */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="para-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number_para')->textInput(['maxlength' => true]) ?>


<?
    	echo TimePicker::widget([
                        'model' => $model,
                        'attribute' => 'begin_time',
                        'pluginOptions' => [
                        	'showSeconds' => true,
                        	'showMeridian' => false,
                        	'minuteStep' => 1,
                        	'secondStep' => 5
                        ]
                    ]);
    ?>

    <?
    	echo TimePicker::widget([
                        'model' => $model,
                        'attribute' => 'end_time',
                        'pluginOptions' => [
                        	'showSeconds' => true,
                        	'showMeridian' => false,
                        	'minuteStep' => 1,
                        	'secondStep' => 5
                        ]
                    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

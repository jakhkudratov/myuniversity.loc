<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ScienceTable */

$this->title = 'Create Science Table';
$this->params['breadcrumbs'][] = ['label' => 'Science Tables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="science-table-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'roomSelect' => 0,
        'rooms' => $rooms,
        'lessonSelect' => 0,
        'lessons' => $lessons, 
        'paras' => $paras,
        'paraSelect' => 0,
        'weekDaySelect' => 0,
        'weekDays' => $weekDays,

    ]) ?>

</div>

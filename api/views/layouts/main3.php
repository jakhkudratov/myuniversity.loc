<?php

/* @var $this \yii\web\View */
/* @var $content string */

//use backend\assets\PublicAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;

//PublicAsset::register($this);
//dmstr\web\AdminLteAsset::register($this);

?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
  <head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>


    <!-- Bootstrap core CSS -->

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <?php $this->head() ?>

  </head>
  <body>


    
    
<?php $this->beginBody() ?>


  <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">My University</a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
  

  <?
    if (Yii::$app->user->isGuest) {
        
      $loginUrl =  Url::to(['/site/login']);  
  ?>
<ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="<?=$loginUrl?>">Login</a>
    </li>
  </ul>

  <?
}else{

    $logOutUrl = Url::to(['/site/logout']);
    $logOut =  Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    

    echo $logOut;
  ?>

<!-- <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="<?= $logOutUrl?>">LogOut(<?= Yii::$app->user->identity->username ?>)</a>
    </li>
  </ul> -->
<?
}
?>
</nav>

<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="sidebar-sticky pt-3">
        <ul class="nav flex-column">
          <!-- <li class="nav-item">
            <a class="nav-link active" href="#">
              <span data-feather="home"></span>
              Dashboard <span class="sr-only">(current)</span>
            </a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/student'])?>">
              <span data-feather="file"></span>
              Students
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/teacher'])?>">
              <span data-feather="shopping-cart"></span>
              Teachers
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/faculty'])?>">
              <span data-feather="users"></span>
              Faculties
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/cafedry'])?>">
              <span data-feather="bar-chart-2"></span>
              Cafedries
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/science'])?>">
              <span data-feather="layers"></span>
              Sciences
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/groups'])?>">
              <span data-feather="layers"></span>
              Groups
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/user'])?>">
              <span data-feather="layers"></span>
              Users
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/week-days'])?>">
              <span data-feather="layers"></span>
              Week days
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/rooms'])?>">
              <span data-feather="layers"></span>
              Rooms
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/para'])?>">
              <span data-feather="layers"></span>
              Paralar
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/teach-science'])?>">
              <span data-feather="layers"></span>
              Teacher and science
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/lesson'])?>">
              <span data-feather="layers"></span>
              Lessons
            </a>
          </li>


          <li class="nav-item">
            <a class="nav-link" href="<?= Url::to(['/science-table'])?>">
              <span data-feather="layers"></span>
              Dars jadvali
            </a>
          </li>

        </ul>

        <!-- <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Saved reports</span>
          <a class="d-flex align-items-center text-muted" href="#" aria-label="Add a new report">
            <span data-feather="plus-circle"></span>
          </a>
        </h6>
        <ul class="nav flex-column mb-2">
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="file-text"></span>
              Current month
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="file-text"></span>
              Last quarter
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="file-text"></span>
              Social engagement
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <span data-feather="file-text"></span>
              Year-end sale
            </a>
          </li>
        </ul>
         -->
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
        
      

        <?= $content ?>
      <div class="d-flex justify-content-center align-items-center  w-100">
        
    </div>
    </main>

    
  </div>
</div>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

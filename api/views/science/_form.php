<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Science */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="science-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'science_name')->textInput(['maxlength' => true]) ?>
	<?= Html::dropDownList('cafedry_id', 0, $cafedries, ['class' => 'form-control']) ?>
    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

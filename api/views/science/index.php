<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ScienceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sciences';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="science-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Science', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'science_name',
            //'cafedry_id',
            [
                'attribute'=> 'cafedry_id',
                'value' => 'cafedry.cafedry_name',
                'format' => 'raw'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

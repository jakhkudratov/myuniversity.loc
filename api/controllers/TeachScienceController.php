<?php

namespace backend\controllers;

use Yii;
use common\models\TeachScience;
use common\models\TeachScienceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use common\models\Teacher;
use common\models\Science;
use yii\filters\AccessControl;

/**
 * TeachScienceController implements the CRUD actions for TeachScience model.
 */
class TeachScienceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            [
               'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    
                ], 
            ],
        ];
    }

    /**
     * Lists all TeachScience models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TeachScienceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TeachScience model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TeachScience model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TeachScience();

        $sciences = ArrayHelper::map(Science::find()->all(), 'id', 'science_name');
        $teachers = ArrayHelper::map(Teacher::find()->all(), 'id', 'full_name');

        if (!empty(Yii::$app->request->post())) {
            $model->science_id = Yii::$app->request->post()['science_id'];
            $model->teacher_id = Yii::$app->request->post()['teacher_id'];
            $science = Science::findOne($model->science_id);
            $teacher = Teacher::findOne($model->teacher_id);
            $model->full_name = $science->science_name . '_' . $teacher->full_name;

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'sciences' => $sciences,
            'teachers' => $teachers
        ]);
    }

    /**
     * Updates an existing TeachScience model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $sciences = ArrayHelper::map(Science::find()->all(), 'id', 'science_name');
        $teachers = ArrayHelper::map(Teacher::find()->all(), 'id', 'full_name');

        if (!empty(Yii::$app->request->post())) {
            $model->science_id = Yii::$app->request->post()['science_id'];
            $model->teacher_id = Yii::$app->request->post()['teacher_id'];
            $science = Science::findOne($model->science_id);
            $teacher = Teacher::findOne($model->teacher_id);
            $model->full_name = $science->science_name . '_' . $teacher->full_name;
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'sciences' => $sciences,
            'teachers' => $teachers
        ]);
    }

    /**
     * Deletes an existing TeachScience model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TeachScience model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TeachScience the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TeachScience::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

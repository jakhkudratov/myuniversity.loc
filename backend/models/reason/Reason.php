<?php

namespace backend\models\reason;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "reason".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $desc
 *
 * @property Objections[] $objections
 */
class Reason extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reason';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['desc'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'desc' => 'Desc',
        ];
    }

    /**
     * Gets query for [[Objections]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjections()
    {
        return $this->hasMany(Objections::className(), ['reason_id' => 'id']);
    }

    public static function getAll()
    {
        $array = self::find()->asArray()->all();
        return ArrayHelper::map($array, 'id', 'title');
    }
}

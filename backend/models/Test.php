<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "test".
 *
 * @property int $id
 * @property int|null $test_group_id
 * @property string|null $question
 * @property int|null $answer_id
 *
 * @property Answer[] $answers
 * @property Answer $answer
 * @property TestGroup $testGroup
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['test_group_id', 'answer_id'], 'integer'],
            [['question'], 'string'],
            [['answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Answer::className(), 'targetAttribute' => ['answer_id' => 'id']],
            [['test_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestGroup::className(), 'targetAttribute' => ['test_group_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_group_id' => 'Test Group ID',
            'question' => 'Question',
            'answer_id' => 'Answer ID',
        ];
    }

    /**
     * Gets query for [[Answers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answer::className(), ['test_id' => 'id']);
    }

    /**
     * Gets query for [[Answer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(Answer::className(), ['id' => 'answer_id']);
    }

    /**
     * Gets query for [[TestGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestGroup()
    {
        return $this->hasOne(TestGroup::className(), ['id' => 'test_group_id']);
    }
}

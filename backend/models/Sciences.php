<?php

namespace backend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sciences".
 *
 * @property int $id
 * @property string|null $photo
 * @property string|null $name
 * @property string|null $title
 * @property string|null $slug
 * @property integer $created_at
 * @property integer $updated_at
 */
class Sciences extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sciences';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['photo', 'name', 'title', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photo' => 'Photo',
            'name' => 'Name',
            'title' => 'Title',
            'slug' => 'Slug',
        ];
    }

    public function getArticlesCount()
    {
        $count = Article::find()
            ->where([
                'science_id' => $this->id
            ])->count();
        return $count ? $count : 0;
    }

    public static function getSciencesList()
    {
        $array = self::find()->asArray()->all();
        return ArrayHelper::map($array, 'id', 'name');
    }
}

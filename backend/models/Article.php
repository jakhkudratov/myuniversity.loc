<?php

namespace backend\models;

use common\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $desc
 * @property int|null $science_id
 * @property string|null $main_part
 * @property string|null $photo
 * @property integer $user_id
 * @property integer $views
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Sciences $science
 * @property User $user
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['science_id', 'user_id'], 'integer'],
            [['main_part', 'photo'], 'string'],
            [['title', 'desc'], 'string', 'max' => 255],
            [['science_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sciences::className(), 'targetAttribute' => ['science_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'desc' => 'Desc',
            'science_id' => 'Science ID',
            'main_part' => 'Main Part',
        ];
    }

    public function getSciencesList()
    {
        $sciences = Sciences::find()->asArray()->all();
        return ArrayHelper::map($sciences, 'id', 'name');

    }

    /**
     * Gets query for [[Science]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScience()
    {
        return $this->hasOne(Sciences::className(), ['id' => 'science_id']);
    }

    /**
     * Gets query for [[Science]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}

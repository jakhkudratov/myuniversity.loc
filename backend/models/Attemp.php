<?php

namespace backend\models;

use backend\models\result\AllResults;
use common\models\User;
use frontend\models\ResultForm;
use Illuminate\Support\Collection;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "attemp".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $test_group_id
 * @property int|null $right_answers
 * @property int|null $wrong_answers
 * @property string|null $session_id
 * @property integer $status
 * @property integer $science_id
 *
 * @property TestGroup $testGroup
 * @property User $user
 * @property Sciences $science
 * @property AttempItems[] $attempItems
 */
class Attemp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attemp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'test_group_id', 'right_answers', 'wrong_answers'], 'integer'],
            [['test_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestGroup::className(), 'targetAttribute' => ['test_group_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }


    const STATUS_START = 0;
    const STATUS_FINISH = 1;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'test_group_id' => 'Test Group ID',
            'right_answers' => 'Right Answers',
            'wrong_answers' => 'Wrong Answers',
        ];
    }

    /**
     * Gets query for [[TestGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestGroup()
    {
        return $this->hasOne(TestGroup::className(), ['id' => 'test_group_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    /**
     * Gets query for [[Sciences]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScience()
    {
        return $this->hasOne(Sciences::className(), ['id' => 'science_id']);
    }

    /**
     * Gets query for [[AttempItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAttempItems()
    {
        return $this->hasMany(AttempItems::className(), ['attemp_id' => 'id']);
    }

    public static function getStatistics($id)
    {
        $attemps = self::find()
            ->where([
                'science_id' => $id,
                'status' => self::STATUS_FINISH
            ])
            ->orderBy(['id'=> SORT_DESC])
            ->asArray()
            ->limit(200)
            ->all();

        $data = [];

        foreach ($attemps as $attemp)
        {
            $data[$attemp['user_id']][] = $attemp;
        }
        //dd($data);
        $result = [];
        foreach ($data as $key => $value)
        {
            $user = User::findOne($key);

            if (empty($user))
                continue;

            $rights = 0;
            $wrongs = 0;
            $attemps = 0;
            foreach ($value as $val)
            {
                $rights += (int)$val['right_answers'];
                $wrongs += (int)$val['wrong_answers'];
                $attemps ++;
            }
            $res = new ResultForm();
            $res->attemps = $attemps;
            $res->right = $rights;
            $res->wrong = $wrongs;
            $res->username = $user->username;
            $res->nisbat = $rights/($rights + $wrongs)*100;
            $result[]=$res;
        }
        return $result;

    }

    public function afterSave($insert, $changedAttributes)
    {
        if (isset($changedAttributes['status']))
        {
            if ($this->status === self::STATUS_FINISH)
            {
                $all_result = AllResults::find()
                    ->where([
                        'user_id' => $this->user_id,
                        'science_id' => $this->science_id
                    ])->one();
                if ($all_result === null)
                {
                    $all_result = new AllResults();
                    $all_result->user_id = $this->user_id;
                    $all_result->science_id = $this->science_id;
                    $all_result->wrong_answers = 0;
                    $all_result->right_answers = 0;
                    $all_result->result = 0;
                    $all_result->attemps = 0;
                    $all_result->save();
                }

                $all_result->attemps ++;
                $all_result->wrong_answers += $this->wrong_answers;
                $all_result->right_answers += $this->right_answers;
                $all_result->result = ($all_result->right_answers/($all_result->wrong_answers + $all_result->right_answers)) *100;
                $all_result->save();

            }
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}

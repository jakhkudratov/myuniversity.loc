<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "teacher".
 *
 * @property int $id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property int|null $science_id
 *
 * @property Sciences $science
 */
class Teacher extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'teacher';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['science_id'], 'integer'],
            [['first_name', 'last_name'], 'string', 'max' => 255],
            [['science_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sciences::className(), 'targetAttribute' => ['science_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'science_id' => 'Science ID',
        ];
    }

    /**
     * Gets query for [[Science]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScience()
    {
        return $this->hasOne(Sciences::className(), ['id' => 'science_id']);
    }
}

<?php

namespace backend\models;

use backend\models\extra\Sequence;
use backend\models\product\Element;
use backend\modules\translatemanager\models\Language;
use backend\modules\translatemanager\models\LanguageSource;
use backend\modules\translatemanager\models\LanguageTranslate;
use Yii;

/**
 * This is the model class for table "signals".
 *
 * @property int $id
 * @property float|null $time
 * @property float|null $amplituda
 */
class Signals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'signals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['time', 'amplituda'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'amplituda' => 'Amplituda',
        ];
    }

    public function importFromExcel($objPHPExcel)
    {
        /** @var \PHPExcel_Worksheet  $sheet */
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        //$rowDataWithOptions = $sheet->rangeToArray('A' . 1 . ':' . $highestColumn . 1, NULL, TRUE, FALSE);

        echo '<pre>';
        //var_dump($highestRow);die();
        for ($row = 2; $row <= $highestRow; $row += 1) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $signals = new Signals();
            $signals->time = $rowData[0][0];
            $signals->amplituda = $rowData[0][2];
            $signals->save();

        }
        die();

        return true;
    }
}

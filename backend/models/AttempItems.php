<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "attemp_items".
 *
 * @property int $id
 * @property int|null $attemp_id
 * @property int|null $test_id
 * @property int|null $answer_id
 * @property integer $status
 *
 * @property Answer $answer
 * @property Attemp $attemp
 * @property Test $test
 */
class AttempItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attemp_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attemp_id', 'test_id', 'answer_id', 'status'], 'integer'],
            [['answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Answer::className(), 'targetAttribute' => ['answer_id' => 'id']],
            [['attemp_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attemp::className(), 'targetAttribute' => ['attemp_id' => 'id']],
            [['test_id'], 'exist', 'skipOnError' => true, 'targetClass' => Test::className(), 'targetAttribute' => ['test_id' => 'id']],
        ];
    }

    const STATUS_NOT_CHECKED = -1;
    const STATUS_CHECKED = 1;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attemp_id' => 'Attemp ID',
            'test_id' => 'Test ID',
            'answer_id' => 'Answer ID',
        ];
    }

    /**
     * Gets query for [[Answer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(Answer::className(), ['id' => 'answer_id']);
    }

    /**
     * Gets query for [[Attemp]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAttemp()
    {
        return $this->hasOne(Attemp::className(), ['id' => 'attemp_id']);
    }

    /**
     * Gets query for [[Test]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTest()
    {
        return $this->hasOne(Test::className(), ['id' => 'test_id']);
    }
}

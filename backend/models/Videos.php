<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "videos".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $desc
 * @property int|null $science_id
 * @property string|null $link
 * @property string|null $photo
 *
 * @property Sciences $science
 */
class Videos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'videos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['science_id'], 'integer'],
            [['link'], 'string'],
            [['title', 'desc', 'photo'], 'string', 'max' => 255],
            [['science_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sciences::className(), 'targetAttribute' => ['science_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'desc' => 'Desc',
            'science_id' => 'Science ID',
            'link' => 'Link',
        ];
    }

    /**
     * Gets query for [[Science]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getScience()
    {
        return $this->hasOne(Sciences::className(), ['id' => 'science_id']);
    }


    public function getSciencesList()
    {
        $sciences = Sciences::find()->asArray()->all();
        return ArrayHelper::map($sciences, 'id', 'name');

    }
}

<?php

namespace backend\models\sliders;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "slider_items".
 *
 * @property integer $id
 * @property integer $slider_id
 * @property string $title
 * @property string $image
 * @property string $mobile_img
 * @property string $link
 * @property string $position
 * @property string $align
 * @property string $color
 * @property string $description
 * @property string $button_bg_color
 * @property string $button_font_color
 * @property string $button_font_hover_color
 * @property integer $weight
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Sliders $slider
 */
class SliderItems extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider_items';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slider_id', 'title', 'image', 'mobile_img',], 'required'],
            [['slider_id', 'weight', 'status', 'created_at', 'updated_at'], 'integer'],
            [['description', 'button_bg_color', 'button_font_color', 'button_font_hover_color'], 'string'],
            [['title', 'image','mobile_img', 'link', 'position', 'align', 'color'], 'string', 'max' => 255],
            [['weight'], 'default', 'value' => 0],
            [['status'], 'default', 'value' => self::STATUS_INACTIVE],
            [['position'], 'default', 'value' => self::POSITION_MIDDLE],
            [['align'], 'default', 'value' => self::ALIGN_LEFT],
            [['color'], 'default', 'value' => '#5E6E7C'],
            [['slider_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sliders::className(), 'targetAttribute' => ['slider_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('model', 'ID'),
            'slider_id' => Yii::t('model', 'Slider ID'),
            'title' => Yii::t('model', 'Title'),
            'image' => Yii::t('model', 'Image'),
            'link' => Yii::t('model', 'Link'),
            'position' => Yii::t('model', 'Position').' ↕',
            'align' => Yii::t('model', 'Align') .' ↔️',
            'color' => Yii::t('model', 'Color'),
            'description' => Yii::t('model', 'Description'),
            'weight' => Yii::t('model', 'Weight'),
            'status' => Yii::t('model', 'Status'),
            'created_at' => Yii::t('model', 'Created At'),
            'updated_at' => Yii::t('model', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlider()
    {
        return $this->hasOne(Sliders::className(), ['id' => 'slider_id']);
    }

    /**
     * Status
     */
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getStatusArray($status = null)
    {
        $array = [
            self::STATUS_INACTIVE => Yii::t('model', 'Inactive'),
            self::STATUS_ACTIVE => Yii::t('model', 'Active'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getStatusName()
    {
        $array = [
            self::STATUS_ACTIVE => '<span class="text-bold text-green">' . self::getStatusArray(self::STATUS_ACTIVE) . '</span>',
            self::STATUS_INACTIVE => '<span class="text-bold text-red">' . self::getStatusArray(self::STATUS_INACTIVE) . '</span>',
        ];

        return isset($array[$this->status]) ? $array[$this->status] : '';
    }

    /**
     * Status
     */
    const POSITION_TOP = 'top';
    const POSITION_MIDDLE = 'middle';
    const POSITION_BOTTOM = 'bottom';

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getPositionArray($status = null)
    {
        $array = [
            self::POSITION_TOP => Yii::t('model', 'Top'),
            self::POSITION_MIDDLE => Yii::t('model', 'Middle'),
            self::POSITION_BOTTOM => Yii::t('model', 'Bottom'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getPositionName()
    {
        $array = [
            self::POSITION_TOP => '<span class="text-bold">' . self::getPositionArray(self::POSITION_TOP) . '</span>',
            self::POSITION_MIDDLE => '<span class="text-bold ">' . self::getPositionArray(self::POSITION_MIDDLE) . '</span>',
            self::POSITION_BOTTOM => '<span class="text-bold ">' . self::getPositionArray(self::POSITION_BOTTOM) . '</span>',
        ];

        return isset($array[$this->position]) ? $array[$this->position] : $this->position;
    }

    /**
     * Status
     */
    const ALIGN_LEFT = 'left';
    const ALIGN_CENTER = 'bottom';
    const ALIGN_RIGHT = 'right';

    /**
     * Status Array
     * @param integer|null $status
     * @return array|string
     */
    public static function getAlignArray($status = null)
    {
        $array = [
            self::ALIGN_LEFT => Yii::t('model', 'Left'),
            self::ALIGN_CENTER => Yii::t('model', 'Bottom'),
            self::ALIGN_RIGHT => Yii::t('model', 'Right'),
        ];

        return $status === null ? $array : $array[$status];
    }

    /**
     * Status Name
     * @return string
     */
    public function getAlignName()
    {
        $array = [
            self::ALIGN_LEFT => '<span class="text-bold">' . self::getAlignArray(self::ALIGN_LEFT) . '</span>',
            self::ALIGN_CENTER => '<span class="text-bold ">' . self::getAlignArray(self::ALIGN_CENTER) . '</span>',
            self::ALIGN_RIGHT => '<span class="text-bold ">' . self::getAlignArray(self::ALIGN_RIGHT) . '</span>',
        ];

        return isset($array[$this->align]) ? $array[$this->align] : $this->align;
    }
}

<?php

namespace backend\models;

use backend\models\TestGroup;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TestGroupSearch represents the model behind the search form of `backend\models\TestGroup`.
 */
class TestGroupSearch extends TestGroup
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'science_id'], 'integer'],
            [['title'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TestGroup::find()
            ->andWhere([
                'not', ['status' => TestGroup::STATUS_DELETED]
            ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'science_id' => $this->science_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);
        //dd($query);
        return $dataProvider;
    }
}

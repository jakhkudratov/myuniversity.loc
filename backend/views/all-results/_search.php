<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\result\AllResultsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="all-results-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'science_id') ?>

    <?= $form->field($model, 'right_answers') ?>

    <?= $form->field($model, 'wrong_answers') ?>

    <?php // echo $form->field($model, 'result') ?>

    <?php // echo $form->field($model, 'attemps') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

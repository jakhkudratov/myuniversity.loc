<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\result\AllResultsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'All Results';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="all-results-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create All Results', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'science_id',
            'right_answers',
            'wrong_answers',
            'result',
            'attemps',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

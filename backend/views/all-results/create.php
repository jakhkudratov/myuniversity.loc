<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\result\AllResults */

$this->title = 'Create All Results';
$this->params['breadcrumbs'][] = ['label' => 'All Results', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="all-results-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

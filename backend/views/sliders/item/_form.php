<?php

use backend\models\sliders\SliderItems;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\elfinder\InputFile;

/* @var $this yii\web\View */
/* @var $model backend\models\sliders\SliderItems */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-items-form box-body">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-6 col-md-10">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>


            <?= $form->field($model, 'image')->widget(InputFile::className(), [
                'controller' => 'elfinder',
                'path' => '/sliders',
                'filter' => 'image',
                'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options' => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-warning'],
                'buttonName' => '<i class="fas fa-camera"></i>',
            ]) ?>

            <?= $form->field($model, 'mobile_img')->widget(InputFile::className(), [
                'controller' => 'elfinder',
                'path' => '/sliders',
                'filter' => 'image',
                'template' => '<div class="input-group">{input}<span class="input-group-btn">{button}</span></div>',
                'options' => ['class' => 'form-control'],
                'buttonOptions' => ['class' => 'btn btn-warning'],
                'buttonName' => '<i class="fas fa-camera"></i>',
            ]) ?>

            <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'color')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'button_font_hover_color')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'button_font_color')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'button_bg_color')->textInput(['maxlength' => true]) ?>
                </div>
                <!--<div class="col-md-4">
                    <?/*= $form->field($model, 'position')->dropDownList(SliderItems::getPositionArray()) */?>
                </div>-->
                <div class="col-md-4">
                    <?= $form->field($model, 'align')->dropDownList(SliderItems::getAlignArray()) ?>
                </div>
            </div>

            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'weight')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2 col-sm-4  col-xs-6">
            <?= $form->field($model, 'status')->dropDownList($model->getStatusArray()) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('views', 'Create') : Yii::t('views', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

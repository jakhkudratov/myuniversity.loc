<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\AttempItems */

$this->title = 'Create Attemp Items';
$this->params['breadcrumbs'][] = ['label' => 'Attemp Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attemp-items-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Sciences */

$this->title = 'Create Sciences';
$this->params['breadcrumbs'][] = ['label' => 'Sciences', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sciences-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

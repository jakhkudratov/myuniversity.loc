<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Sciences */

$this->title = 'Update Sciences: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Sciences', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="sciences-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

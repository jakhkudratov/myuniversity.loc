<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TestGroup */

$this->title = 'Create Test Group';
$this->params['breadcrumbs'][] = ['label' => 'Test Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TestGroup */

$this->title = 'Update Test Group: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Test Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="test-group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

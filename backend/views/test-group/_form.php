<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TestGroup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="test-group-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'science_id')->dropDownList(\backend\models\Sciences::getSciencesList()) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use kartik\file\FileInput;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\TestGroup */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Test Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="test-group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>

        <?
         Modal::begin([
                        'header' => '<h4>' . Yii::t('views', 'Excel import') . '</h4>',
                        'toggleButton' => [
                            'label' => Yii::t('views', 'Excel import'),
                            'class' => 'btn btn-warning'
                        ],
                    ]);


                    $this->registerJs("
                    var uploas, uploadedFiles = {}, deletedFiles = [],
                    uploaded = document.getElementById('upload_files'),
                    deleted = document.getElementById('deleted_images');");

                    echo Html::beginForm(['excel-import'], 'POST')
                        .Html::hiddenInput( 'test_group_id', Yii::$app->request->queryParams['id'])
                        .Html::hiddenInput( 'name_excel', null,['id' => 'upload_files'])
                        . FileInput::widget([
                            'name' => 'file',
                            'options' => [
                                'multiple' => true
                            ],
                            'pluginOptions' => [
                                'uploadUrl' => Url::to(['upload-file']),
                                'deleteUrl' => Url::to(['delete-image']),
                                //'allowedPreviewExtensions' => ['jpg', 'jpeg', 'png', 'svg', 'zip', 'rar', 'mp4', 'avi', 'mkv'],
                                'maxFileSize' => 10240,
                                'maxFileCount' => 1,
                                'fileActionSettings' => [
                                    'removeIcon' => '<i class="fas fa-trash"></i>',
                                    'uploadIcon' => '<i class="fas fa-file-upload"></i>',
                                    'zoomIcon' => '<i class="fas fa-search-plus"></i>'
                                ]
                            ],
                            'pluginEvents' => [
                                'fileuploaded' => new JsExpression('function(event, data, previewId) {
                                    console.log(data.response);
                                    uploadedFiles[data.response.name] = data.response.path;
        
                                    uploaded.value = data.response.path;
                                    
                                    console.log(uploaded.value);
                                }'),
                                'filedeleted' => new JsExpression('function(event, key) {
                                    deletedFiles.push(key);
                                    deleted.value = JSON.stringify(deletedFiles);
                                }'),
                                'filesuccessremove' => new JsExpression('function(event, previewId) {
                                    delete uploadedFiles[previewId];
                                    uploaded.value = JSON.stringify(uploadedFiles);
                                }'),

                            ]
                        ])
                        . '<div class="input-group-btn">'
                        . Html::submitButton(Yii::t('views', 'Send'), ['class' => 'btn btn-success'])
                        . '</div>'
                        . Html::endForm();



                    Modal::end();
                ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'science_id',
                'value' => function(\backend\models\TestGroup $model)
                {
                    return $model->science->name;
                }
            ],
            'science_id',
            'title',
        ],
    ]) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            //'test_group_id',
            'question:ntext',
            [
                'attribute' => 'answer_id',
                'value' => function(\backend\models\Test $model)
                {
                    return $model->answer->value;
                }
            ]
        ],
    ]); ?>

</div>

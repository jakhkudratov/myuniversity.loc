<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\reason\ObjectionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Objections';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objections-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Objections', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'test_id',
            'reason_id',
            'comment:ntext',
            'test_group_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\reason\Objections */

$this->title = 'Create Objections';
$this->params['breadcrumbs'][] = ['label' => 'Objections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objections-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Attemp */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attemp-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'test_group_id')->textInput() ?>

    <?= $form->field($model, 'right_answers')->textInput() ?>

    <?= $form->field($model, 'wrong_answers')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

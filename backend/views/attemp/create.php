<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Attemp */

$this->title = 'Create Attemp';
$this->params['breadcrumbs'][] = ['label' => 'Attemps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attemp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

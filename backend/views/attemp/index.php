<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AttempSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attemps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attemp-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Attemp', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'test_group_id',
            'right_answers',
            'wrong_answers',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>

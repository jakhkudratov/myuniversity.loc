<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Signals */

$this->title = 'Create Signals';
$this->params['breadcrumbs'][] = ['label' => 'Signals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="signals-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

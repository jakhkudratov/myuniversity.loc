<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Menu My university', 'options' => ['class' => 'header']],
                     
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'Teachers', 'icon' => 'dashboard', 'url' => ['/teacher']],
                    ['label' => 'Students', 'icon' => 'dashboard', 'url' => ['/student']],
                    ['label' => 'Faculties', 'icon' => 'dashboard', 'url' => ['/faculty']],
                    ['label' => 'Cafedries', 'icon' => 'dashboard', 'url' => ['/cafedry']],
                    ['label' => 'Rooms', 'icon' => 'dashboard', 'url' => ['/rooms']],
                    ['label' => 'Paralar', 'icon' => 'dashboard', 'url' => ['/para']],
                    ['label' => 'Sciences', 'icon' => 'dashboard', 'url' => ['/science']],
                    ['label' => 'Groups', 'icon' => 'dashboard', 'url' => ['/groups']],
                    ['label' => 'Users', 'icon' => 'dashboard', 'url' => ['/user']],
                    ['label' => 'Week Days', 'icon' => 'dashboard', 'url' => ['/week-days']],
                    ['label' => 'Teacher and science', 'icon' => 'dashboard', 'url' => ['/teach-science']],
                    ['label' => 'Lessons', 'icon' => 'dashboard', 'url' => ['/lesson']],
                    ['label' => 'Dars jadvali', 'icon' => 'dashboard', 'url' => ['/science-table']],

                    /*[
                        'label' => 'Some tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],*/
                ],
            ]
        ) ?>

    </section>

</aside>

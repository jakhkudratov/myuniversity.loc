<?php

namespace backend\controllers;

use backend\models\Test;
use common\service\ExcelImportService;
use PHPExcel_IOFactory;
use Yii;
use backend\models\TestGroup;
use backend\models\TestGroupSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * TestGroupController implements the CRUD actions for TestGroup model.
 */
class TestGroupController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TestGroup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TestGroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAll()
    {
        $all = TestGroup::find()->all();
        foreach ($all as $key => $value)
        {
            $value->status = TestGroup::STATUS_NEW;
            $value->save();
        }
    }

    /**
     * Displays a single TestGroup model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $query = Test::find()
            ->where([
                'test_group_id' => $id
            ]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Creates a new TestGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TestGroup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TestGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TestGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = TestGroup::STATUS_DELETED;
        $model->save();
        //$this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TestGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TestGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TestGroup::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    /**
     * Upload Exel File.
     * @return array
     */
    public function actionUploadFile()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = [];

        if ($file_image = UploadedFile::getInstancesByName('file')) {
            foreach ($file_image as $file) {
                $folder = '/product/excel/';
                if (!file_exists(Yii::getAlias('@rootDir').'/uploads/product/excel/')) {
                    mkdir(Yii::getAlias('@rootDir').'/uploads/product/excel/', 0777, true);
                }
                $ext = pathinfo($file->name, PATHINFO_EXTENSION);
                $name = pathinfo($file->name, PATHINFO_FILENAME);
                $generateName = Yii::$app->security->generateRandomString();
                $path = Yii::getAlias('@uploadsPath') . $folder . $generateName . ".{$ext}";
                $file->saveAs($path);
                $data = [
                    'generate_name' => $generateName,
                    'name' => $name,
                    'path' => Yii::getAlias('@uploadsUrl') . $folder . $generateName . ".{$ext}"
                ];
            }
        }

        return $data;
    }


    /**
     * Delete Product Image.
     * @return object
     */
    public function actionDeleteImage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ($post = Yii::$app->request->post()) ? $post['key'] : null;
    }


    /**
     * Import In Excel.
     * @return mixed
     */
    public function actionExcelImport()
    {
        if ($post = Yii::$app->request->post()) {
            $excel = Yii::getAlias('@rootDir') . $post['name_excel'];
            // dd($excel);
            try {
                $inputFileType = PHPExcel_IOFactory::identify($excel);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($excel);
                //dd($objPHPExcel);
            } catch (\Exception $e) {
                die('Error');
            }
            $test_group_id = $post['test_group_id'];
            if (ExcelImportService::importProductElements($objPHPExcel, $test_group_id))

                return $this->redirect(['index']);
        }

        return $this->redirect(['index']);
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "test".
 *
 * @property int $id
 * @property string $tesname
 * @property string|null $testpass
 * @property int|null $type
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tesname'], 'required'],
            [['type'], 'integer'],
            [['tesname'], 'string', 'max' => 255],
            [['testpass'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tesname' => 'Tesname',
            'testpass' => 'Testpass',
            'type' => 'Type',
        ];
    }
}

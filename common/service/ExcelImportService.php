<?php

namespace common\service;

use backend\models\Answer;
use backend\models\Test;
use PHPExcel;
use PHPExcel_Worksheet;

class ExcelImportService
{

    /**
     * @param PHPExcel $objPHPExcel
     * @return bool
     */
    public static function importProductElements($objPHPExcel, $test_group_id)
    {

        /** @var PHPExcel_Worksheet  $sheet */
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        for ($row = 2; $row <= $highestRow; $row += 1) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            $test = new Test();
            $test->question = (string)$rowData[0][0];
            $test->test_group_id = $test_group_id;
            if ($test->save())
            {
                $answer1 = new Answer();
                $answer1->value = (string)$rowData[0][1];
                $answer1->test_id = $test->id;
                if ($answer1->save())
                {
                    $test->answer_id = $answer1->id;
                    $test->save();
                }
                else
                {
                    var_dump($answer1->errors);
                    die();
                }

                $answer2 = new Answer();
                $answer2->value = (string)$rowData[0][2];
                $answer2->test_id = $test->id;
                if (!$answer2->save())
                {
                    var_dump($answer2->errors);
                    die();
                }

                $answer3 = new Answer();
                $answer3->value = (string)$rowData[0][3];
                $answer3->test_id = $test->id;
                if (!$answer3->save())
                {
                    var_dump($answer3->errors);
                    die();
                }

                $answer4 = new Answer();
                $answer4->value = (string)$rowData[0][4];
                $answer4->test_id = $test->id;
                if (!$answer4->save())
                {
                    var_dump($answer4->errors);
                    die();
                }
            }

        }
        return true;
    }

}
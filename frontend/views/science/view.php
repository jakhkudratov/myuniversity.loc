<?php
/* @var $this yii\web\View */
/** @var \backend\models\Sciences $model */
/** @var array $articles */
/** @var \backend\models\Article $article */

use yii\helpers\Url;


//var_dump($model->photo);
?>

<div class="event-area bg-img default-overlay pt-80 pb-100" style="background-image:url(/uploads/main_page/bg-3.jpg);">
    <div class="container">
        <div class="section-title mb-75">
            <h2><span><?= $model->name?></span></h2>
            <p><?= $model->title?></p>
        </div>
        <div class="row">


            <?php
            foreach ($articles as $article)
                {
                    ?>
                    <div class="col-md-4">
                        <div class="single-event event-white-bg">
                            <div class="event-img">
                                <a href="<?= Url::to(['/article/view', 'id' => $article->id])?>"><img src="<?= $article->photo?>" alt="<?= $article->title?>"></a>
                                <div class="event-date-wrap">
                                    <span class="event-date"><?= date('d', $article->updated_at)?></span>
                                    <span><?= date('M', $article->updated_at)?></span>
                                </div>
                            </div>
                            <div class="event-content">
                                <h3><a href="<?= Url::to(['/article/view', 'id' => $article->id])?>"><?= $article->title?></a></h3>
                                <p><?= $article->desc?></p>
                                <div class="event-meta-wrap">
                                    <div class="event-meta">
                                        <i class="fa fa-location-arrow"></i>
                                        <span><?= $article->user->username?></span>
                                    </div>
                                    <div class="event-meta">
                                        <i class="fa fa-clock-o"></i>
                                        <span><?= date('d/m/Y', $article->updated_at)?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?
                }
            ?>



        </div>
    </div>
</div>
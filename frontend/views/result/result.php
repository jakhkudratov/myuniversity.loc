<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $dataProvider_ yii\data\ActiveDataProvider */

?>


<div class="container">
    <div class="table-responsive">
        <?php
            if (!empty($dataProvider_->models))
            {
                ?>

                <div class="article-index">

                    <h1><?= Html::encode("Mutlaq Yetakchilar") ?></h1>

                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider_,
                        // 'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'username',
                            [
                                'attribute' => 'user_id',
                                'label' => 'Foydalanuvchi',
                                'value' => function(\backend\models\result\AllResults $model)
                                {
                                    return $model->user->username;
                                }
                            ],
                            [
                                'attribute' => 'wrong_answers',
                                'label' => 'Xato javoblar'
                            ],
                            [
                                'attribute' => 'right_answers',
                                'label' => 'To\'g\'ri javoblar'
                            ],
                            [
                                'attribute' => 'result',
                                'label' => 'Natija'
                            ],
                            [
                                'attribute' => 'attemps',
                                'label' => 'Urinishlar soni'
                            ]
                        ],
                    ]); ?>

                </div>
        <?php
            }
        ?>
        <div class="article-index">

            <h1><?= Html::encode("Natijalar") ?></h1>

            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'username',
                    [
                        'attribute' => 'user_id',
                        'label' => 'Foydalanuvchi',
                        'value' => function(\backend\models\result\AllResults $model)
                        {
                            return $model->user->username;
                        }
                    ],
                    [
                        'attribute' => 'wrong_answers',
                        'label' => 'Xato javoblar'
                    ],
                    [
                        'attribute' => 'right_answers',
                        'label' => 'To\'g\'ri javoblar'
                    ],
                    [
                        'attribute' => 'result',
                        'label' => 'Natija'
                    ],
                    [
                        'attribute' => 'attemps',
                        'label' => 'Urinishlar soni'
                    ]
                ],
            ]); ?>

        </div>
    </div>
</div>

<?php
/* @var $this yii\web\View */
/** @var array $sciences */

use yii\helpers\Url;


$js = <<<JS
$('#openModal').on('click', function (){
    alert('fcsdvfd')
    $('#exampleModal').modal('show');
})
JS;

$this->registerJs($js);
?>


<div class="container">
    <div class="row">
        <?php
        foreach ($sciences as $science)
        {
            ?>
            <div class="col-md-3" >
                <div class="single-course">
                    <div class="course-img">
                        <a href="<?= Url::to(['/result/result', 'id' => $science->id])?>"><img class="animated" src="<?= $science->photo?>" alt=""></a>
                    </div>
                    <div class="course-content">
                        <h4><a href="<?= Url::to(['/result/result', 'id' => $science->id])?>"><?= $science->name?></a></h4>
                        <p><?= $science->title?></p>
                    </div>
                    <div class="course-position-content">
                        <div class="credit-duration-wrap">
                            <div class="sin-credit-duration">
                                <i class="fa fa-diamond"></i>
                                <span>Darslar : <?= $science->getArticlesCount()?></span>
                            </div>
                        </div>
                        <div class="course-btn">
                            <a class="default-btn" href="<?=Url::to(['/result/result', 'id' => $science->id]) ?>">Hoziroq qo'shilish</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>

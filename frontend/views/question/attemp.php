<?php
/* @var $this yii\web\View */
/** @var \backend\models\Test $test */
/** @var \frontend\models\TestForm $model */
/** @var array $answers */


use yii\helpers\Html;
use yii\helpers\Url;

$array = [
        0 => ' A) ',
        1 => ' B) ',
        2 => ' C) ',
        3 => ' D) ',
];
$k=1;

$js = <<<JS
    window.setTimeout(document.testForm.submit.bind(document.testForm), 60000);
JS;
//$this->registerJs($js);

?>
<div class="event-area bg-img pt-80 pb-100" style="background-image:url(/uploads/main_page/bg-3.jpg);">


    <div class="container">
        <div class="row ml-2" id="question">
            <?php echo Html::beginForm('', 'post', ['name' => 'testForm', 'class' => "w-100"]) ?>

            <?php
            /** @var array $data */
            foreach ($data as $key => $test)
            {
                $number = ++$key;
                ?>
                <div class="test-box-attemp">
                    <div class="form-group question-list mt-3 w-100">
                        <ul class="tariffs-checkbox-list row w-100 test-ul">
                            <?php $question = $test['test']['question'] ; ?>
                            <?php $radioData = \yii\helpers\ArrayHelper::map($test['answers'], 'id', 'value') ; ?>
                            <h2 class="d-block w-100"><?= $number?>. ) <?= $question?></h2><br>
                            <?php echo Html::radioList('answer_id', null, $radioData, [
                                'item' => function ($index, $label, $name, $checked, $value) use ($test, $key, $array) {

                                    $return = "<li class='col-md-10 period-label d-block'>
                                              <label for='{id}' class='radio d-flex align-items-center' style='padding-left: 30px'>
                                             <input type='radio' id='{id}' name='{name}' value='{value}'> <span class='pl-2'>{index} {label}</span> 
                                                <span class='checkmark'><span>
                                                </label>
                                             </li>";
                                    return strtr($return, [
                                        '{id}' => 'question_'.$key.$index,
                                        '{name}' => 'Test['.$test['test']['id'].']',
                                        '{label}' => $label,
                                        '{value}' => $value,
                                        '{index}' => $array[$index]. '. '
                                    ]);
                                },
                                'tag' => false,
                            ]) ?>
                        </ul>
                    </div>
                </div>
                <?php

            }
            ?>

            <div class="form-group">
                <?php echo Html::submitButton(Yii::t('installment', 'Submit'), ['class' => 'btn text-center btn-success', 'name' => 'send-installment']) ?>
            </div>
            <?php echo Html::endForm() ?>
        </div>
    </div>

</div>
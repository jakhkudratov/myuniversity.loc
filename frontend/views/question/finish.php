<?php
/* @var $this yii\web\View */
/** @var \backend\models\Attemp $model */

use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;


?>
<!--
<style>
    #chartdiv {
        width: 100%;
        height: 500px;
    }

</style>-->

<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->

<?php

$js = <<<JS
am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
        var chart = am4core.create("chartdiv", am4charts.PieChart);

// Add data
        chart.data = [ {
            "country": "Right answers",
            "litres": {rights},
            "color" : '#1DE9B6'
        }, {
            "country": "Wrong answers",
            "litres": {wrongs},
            "color" : '#DD2C00'

        }];

// Add and configure Series
        var pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "litres";
        pieSeries.dataFields.category = "country";
        pieSeries.slices.template.propertyFields.fill = "color";
        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.slices.template.strokeWidth = 2;
        pieSeries.slices.template.strokeOpacity = 1;
        

// This creates initial animation
        pieSeries.hiddenState.properties.opacity = 1;
        pieSeries.hiddenState.properties.endAngle = -90;
        pieSeries.hiddenState.properties.startAngle = -90;

    }); // end am4core.ready()
JS;

$js = strtr($js, [
    '{rights}' => $model->right_answers,
    '{wrongs}' => $model->wrong_answers
]);
$this->registerJs($js);

$modal_js = <<<JS

$(document).ready(function () {
        $('.modalButton').on('click', function (event){
            var test_id = $(this).data('test');
            openModal(test_id);
        })
    
});
function submitModal()
{       var form = $('#resonForm');
        if (form.find('.has-error').length) 
        {
            return false;
        }
        
        $.ajax({
                url    : '/question/reason',
                type   : 'post',
                data   : form.serialize(),
                success: function (response) 
                {
                    
                    if (response === true){
                        cloaseModal();
                        $('#resonForm')['0'].reset();
                        alert('Murojaatingiz qabul qilindi!');
                    }
                    else{
                        cloaseModal();
                        $('#resonForm')['0'].reset();
                        alert('Xatolik kuzatildi!');
                    }
                            
                        },
                error  : function () 
                {
                    cloaseModal();
                    alert('Eror');
                }
        });
        return false;
}

function alertsuccess()
{
    $('#successAlert').alert('show');
}

function alertDanger()
{
    $('.alert-danger').alert('show');
}

function openModal(test_id)
{
    $('#reasonTest').val(test_id);
    $('#reasonModal').modal('show');
}

function cloaseModal()
{
    $('#reasonModal').modal('hide');
}
$('#submitModal').on('click', function (){
    submitModal();
})


JS;

$this->registerJs($modal_js);

?>
<div class="event-area bg-img pt-80 pb-100" style="background-image:url(/uploads/main_page/bg-3.jpg);">



    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h1><?= $model->testGroup->science->name?></h1>
                <h2 class="font-weight-bold"><?= $model->testGroup->title?></h2>
                <h3 class="font-weight-bold text-success"><?= $model->right_answers?></h3>
                <h3 class="font-weight-bold text-danger"><?= $model->wrong_answers ?></h3>
            </div>
            <div class="col-md-8">
                <div id="chartdiv"></div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <?php

            /** @var array $items */
            foreach ($items as $key => $item)
            {
                ?>
                <div class="col-md-12">
                    <div class="test-box font-weight-bold">

                        <div class="d-flex align-items-center justify-content-between">
                            <h2><?= $key+=1?> .) <?= $item['question']?></h2>
                            <span><button class="btn btn-transparent modalButton" data-test="<?= $item['test_id']?>"><i class="fas fa-exclamation-triangle"></i></button></span>
                        </div>

                        <p>Sizning Javobingiz</p>
                        <p class="font-weight-bold <?= ($item['is_right'])?'bg-success-local':'bg-danger-local'?>"><?= $item['answer']?></p>
                        <p>To'g'ri javob</p>
                        <p class="text-success bg-success-local"><?= $item['right_answer']?></p>
                    </div>

                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-sm" id="reasonModal" tabindex="-1" role="dialog" aria-labelledby="reasonModal" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="reasonModal">Отправить сообщение об ошибке *</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <?php

                echo Html::beginForm('', 'post', ['name' => 'reason', 'id' => 'resonForm']);


                ?>

                    <input type="hidden" name="test_id" id="reasonTest">
                    <div class="form-group">
                        <p>Тема сообщения</p>
                        <?php
                        echo Html::dropDownList('reason_id',null,\backend\models\reason\Reason::getAll(), ['class' => 'form-control'])
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                            echo Html::textarea('comment','', ['class' => 'form-control']);
                        ?>
                    </div>
                <?php
                    echo Html::endForm();
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary" id="submitModal">Отправить</button>
            </div>
        </div>
    </div>
</div>




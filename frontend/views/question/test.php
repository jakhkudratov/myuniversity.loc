<?php
/* @var $this yii\web\View */
/** @var \backend\models\Test $test */
/** @var \frontend\models\TestForm $model */
/** @var array $answers */

$form = \yii\widgets\ActiveForm::begin();

/*var_dump($test->question);
die();*/
$array = [
        0 => ' A) ',
        1 => ' B) ',
        2 => ' C) ',
        3 => ' D) ',
];
$k=1;

?>
    <div class="event-area bg-img default-overlay pt-80 pb-100" style="background-image:url(/uploads/main_page/bg-3.jpg);">
        <div class="container">
            <div class="row">
                <div>
                    <h2><?= $model->raqam?>-Savol : <?echo $test->question?></h2>
                </div>
                <ul class="list-group position-relative">

                    <?= $form->field($model, 'answer_id')->radioList(\yii\helpers\ArrayHelper::map($answers, 'id', 'value'), [
                        'item' => function($index, $label, $name, $checked, $value) use ($array){

                            $return = "<li class='period-label position-relative test-box' style='position: relative'>
                                                  <label for='{$index}' class='radio d-flex align-items-center'>
                                                 <input type='radio' id='{$index}' {$checked} name='{$name}' value='{$value}' class='test-input' /> 
                                                 <span>{$array[$index]}{$label}</span> 
                                                    <span class='checkmark'><span>
                                                    </label>
                                                 </li>";

                            return $return;
                        }
                    ])?>
                </ul>


                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>
    </div>


<?php
    \yii\widgets\ActiveForm::end();
?>
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
    <!--<div class="container" id="container">

        <div class="form-container sign-in-container">
            <?php /*$form = ActiveForm::begin(['id' => 'login-form']); */?>

            <h1>Saytga kirish</h1>
                <div class="social-container">
                    <a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
                    <a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
                    <a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
                </div>
                <span>Akkountizgizga kiring</span>

                <?/*= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'my-input', 'placeholder' => 'username'])->label(false) */?>

                <?/*= $form->field($model, 'password')->passwordInput(['class' => 'my-input', 'placeholder' => 'password'])->label(false) */?>

                <a href="#">Parolingizni unitdingizmi?</a>

                <div class="form-group">
                    <?/*= Html::submitButton('Login', [ 'name' => 'login-button']) */?>
                </div>

                <?php /*ActiveForm::end(); */?>

        </div>
        <div class="overlay-container">
            <div class="overlay">
                <div class="overlay-panel overlay-right">
                    <h1>Salom!</h1>
                    <p>Bilimingizn sinab ko'ring</p>
                </div>
            </div>
        </div>
    </div>-->


<div class="register-area bg-img pt-100 pb-130" style="background-image:url(/uploads/main_page/bg-2.jpg);">
    <div class="container">
        <div class="section-title-2 mb-75 white-text">
            <h2>Hoziroq <span>Saytimizga tashrif buyuring</span></h2>
            <p>Bilimingizni sinab ko'ring</p>
        </div>
        <div class="register-wrap">
            <div id="register-3" class="mouse-bg" style="inset: -6.25485% -0.967413%; z-index: 1;">
                <div class="winter-banner">
                    <img src="/uploads/main_page/regi-1.png" alt="">
                    <div class="winter-content">
                        <span>Bilimingni </span>
                        <h3>Sinab</h3>
                        <span>Ko'r </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-md-8">
                    <div class="register-form">
                        <h4>Hoziroq sinab ko'ring</h4>
                        <?php
                        $form = \yii\widgets\ActiveForm::begin();
                        ?>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="contact-form-style mb-20">
                                    <?= $form->field($model, 'username')->textInput(['placeholder'=> 'Foydalanuvchi nomi'])->label(false)?>
                                    <button class="submit default-btn text-uppercase" type="submit">Hoziroq qo'shilish</button>

                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="contact-form-style mb-20">
                                    <?= $form->field($model, 'password')->textInput(['placeholder'=> 'Parolni kiriting'])->label(false)?>

                                </div>
                            </div>

                        </div>
                        <?php
                        \yii\widgets\ActiveForm::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="register-1" class="mouse-bg" style="inset: -12.5097% -1.93483%; z-index: 1;"></div>
    <div id="register-2" class="mouse-bg" style="inset: -12.5097% -1.93483%; z-index: 1;"></div>
</div>

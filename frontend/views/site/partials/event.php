<?php
?>

<div class="event-area bg-img default-overlay pt-130 pb-130" style="background-image:url(/uploads/main_page/bg-3.jpg);">
    <div class="container">
        <div class="section-title mb-75">
            <h2><span>O'quv</span> Mashg'ulotlarimiz</h2>
            <p>O'quv mashg'ulotlarimiz qisqa va  tushunarli olib boriladi. <br>Hoziroq o'quv mashg'ulotlarimizda ishtirok eting va bilimingizni sinab ko'ring </p>
        </div>
        <div class="event-active owl-carousel nav-style-1 owl-loaded owl-drag">

            <?php

            use yii\helpers\Url;

            foreach ($articles as $article)
            {
                ?>
                <div class="owl-item cloned">
                    <div class="single-event event-white-bg">
                        <div class="event-img">
                            <a href="<?= Url::to(['/article/view', 'id' => $article->id])?>"><img src="<?= $article->photo?>" alt="<?= $article->title?>"></a>
                            <div class="event-date-wrap">
                                <span class="event-date"><?= date('d', $article->updated_at)?></span>
                                <span><?= date('M', $article->updated_at)?></span>
                            </div>
                        </div>
                        <div class="event-content">
                            <h3><a href="<?= Url::to(['/article/view', 'id' => $article->id])?>"><?= $article->title?></a></h3>
                            <p><?= $article->desc?></p>
                            <div class="event-meta-wrap">
                                <div class="event-meta">
                                    <i class="fa fa-location-arrow"></i>
                                    <span><?= $article->user->username?></span>
                                </div>
                                <div class="event-meta">
                                    <i class="fa fa-clock-o"></i>
                                    <span><?= date('d/m/Y', $article->updated_at)?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?
            }
            ?>


                    <div class="owl-item cloned" style="width: 370px; margin-right: 30px;"><div class="single-event event-white-bg">
                            <div class="event-img">
                                <a href="#"><img src="/uploads/main_page/event-2.jpg" alt=""></a>
                                <div class="event-date-wrap">
                                    <span class="event-date">10th</span>
                                    <span>Dec</span>
                                </div>
                            </div>
                            <div class="event-content">
                                <h3><a href="#">Global Conference on Business.</a></h3>
                                <p>Pvolupttem accusantium doloremque laudantium, totam erspiciatis unde omnis iste natus error .</p>
                                <div class="event-meta-wrap">
                                    <div class="event-meta">
                                        <i class="fa fa-location-arrow"></i>
                                        <span>Shubastu ,Dadda</span>
                                    </div>
                                    <div class="event-meta">
                                        <i class="fa fa-clock-o"></i>
                                        <span>08:30 am</span>
                                    </div>
                                </div>
                            </div>
                        </div></div>
                    <div class="owl-item cloned" style="width: 370px; margin-right: 30px;"><div class="single-event event-white-bg">
                            <div class="event-img">
                                <a href="#"><img src="/uploads/main_page/event-3.jpg" alt=""></a>
                                <div class="event-date-wrap">
                                    <span class="event-date">1st</span>
                                    <span>Dec</span>
                                </div>
                            </div>
                            <div class="event-content">
                                <h3><a href="#">Academic Conference Maui.</a></h3>
                                <p>Pvolupttem accusantium doloremque laudantium, totam erspiciatis unde omnis iste natus error .</p>
                                <div class="event-meta-wrap">
                                    <div class="event-meta">
                                        <i class="fa fa-location-arrow"></i>
                                        <span>Banasree ,Rampura</span>
                                    </div>
                                    <div class="event-meta">
                                        <i class="fa fa-clock-o"></i>
                                        <span>10:00 am</span>
                                    </div>
                                </div>
                            </div>
                        </div></div>
                    <div class="owl-item cloned" style="width: 370px; margin-right: 30px;"><div class="single-event event-white-bg">
                            <div class="event-img">
                                <a href="#"><img src="/uploads/main_page/event-2.jpg" alt=""></a>
                                <div class="event-date-wrap">
                                    <span class="event-date">1st</span>
                                    <span>Dec</span>
                                </div>
                            </div>
                            <div class="event-content">
                                <h3><a href="#">Social Sciences &amp; Education.</a></h3>
                                <p>Pvolupttem accusantium doloremque laudantium, totam erspiciatis unde omnis iste natus error .</p>
                                <div class="event-meta-wrap">
                                    <div class="event-meta">
                                        <i class="fa fa-location-arrow"></i>
                                        <span>Shubastu ,Badda</span>
                                    </div>
                                    <div class="event-meta">
                                        <i class="fa fa-clock-o"></i>
                                        <span>10:30 am</span>
                                    </div>
                                </div>
                            </div>
                        </div></div>
                    <div class="owl-item active" style="width: 370px; margin-right: 30px;"><div class="single-event event-white-bg">
                            <div class="event-img">
                                <a href="#"><img src="/uploads/main_page/event-1.jpg" alt=""></a>
                                <div class="event-date-wrap">
                                    <span class="event-date">1st</span>
                                    <span>Dec</span>
                                </div>
                            </div>
                            <div class="event-content">
                                <h3><a href="#">Aempor incididunt ut labore ejam.</a></h3>
                                <p>Pvolupttem accusantium doloremque laudantium, totam erspiciatis unde omnis iste natus error .</p>
                                <div class="event-meta-wrap">
                                    <div class="event-meta">
                                        <i class="fa fa-location-arrow"></i>
                                        <span>Mascot Plaza ,Uttara</span>
                                    </div>
                                    <div class="event-meta">
                                        <i class="fa fa-clock-o"></i>
                                        <span>11:00 am</span>
                                    </div>
                                </div>
                            </div>
                        </div></div>



    </div>
</div>

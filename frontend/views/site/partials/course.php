<?php
use yii\helpers\Url;

?>

<div class="course-area bg-img pt-130 pb-10" style="background-image:url(/uploads/main_page/bg-1.jpg);">
    <div class="container">
        <div class="section-title mb-75">
            <h2> <span>Bizning</span> Kurslarimiz</h2>
            <p>Bu fanlardan kerakli o'quv mashg'ulotlari va <br>test jarayonlari doimiy olib boriladi </p>
        </div>
        <div class="course-slider-active nav-style-1 owl-carousel owl-loaded owl-drag">
            <?php
            /** @var array $sciences */
            /** @var \backend\models\Sciences $science */


            foreach ($sciences as $science)
               {

                   ?>
                   <div class="owl-item cloned" style="width: 292.5px;"><div class="single-course">
                           <div class="course-img">
                               <a href="<?= Url::to(['/science/view', 'id' => $science->id])?>"><img class="animated" src="<?= $science->photo?>" alt=""></a>
                           </div>
                           <div class="course-content">
                               <h4><a href="<?= Url::to(['/science/view', 'id' => $science->id])?>"><?= $science->name?></a></h4>
                               <p><?= $science->title?></p>
                           </div>
                           <div class="course-position-content">
                               <div class="credit-duration-wrap">
                                   <div class="sin-credit-duration">
                                       <i class="fa fa-diamond"></i>
                                       <span>Darslar : <?= $science->getArticlesCount()?></span>
                                   </div>
                                   <!--<div class="sin-credit-duration">
                                       <i class="fa fa-clock-o"></i>
                                       <span>Boshlangan vaqti : <?/*= date('d/m/Y', $science->created_at)*/?></span>
                                   </div>-->
                               </div>
                               <div class="course-btn">
                                   <a class="default-btn" href="<?=Url::to(['/science/view', 'id' => $science->id]) ?>">Hoziroq qo'shilish</a>
                               </div>
                           </div>
                       </div>
                   </div>
                   <?

               }
            ?>

        </div>
    </div>

</div>


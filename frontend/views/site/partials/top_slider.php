<?php

use yii\helpers\Url;

$sciences = \backend\models\Sciences::find()->all();

?>

<div class="slider-area">
    <div class="slider-active owl-carousel owl-loaded owl-drag">
        <div class="owl-item cloned" style="width: 1519px;"><div class="single-slider slider-height-1 bg-img" style="background-image:url(/uploads/main_page/slider-1.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-9 col-md-7 col-12 col-sm-12">
                            <div class="slider-content slider-animated-1 pt-230">
                                <h1 class="animated">Kelajakni siz yaratasiz</h1>
                                <p class="animated">Hozirooq bizga qo'shiling! Saytimizda ro'yxatdan o'ting! Online darslarimizda qatnashing va Bilimingizni sinab ko'ring </p>
                                <div class="slider-btn">
                                    <a class="animated default-btn btn-green-color" href="<?= Url::to(['/site/about'])?>">Biz haqimizda</a>
                                    <a class="animated default-btn btn-white-color" href="<?= Url::to(['/site/contact'])?>">Aloqa</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-single-img slider-animated-1">
                        <img class="animated" src="/uploads/main_page/single-slide-1.png" alt="">
                    </div>
                </div>
            </div></div>
        <div class="owl-item cloned" style="width: 1519px;"><div class="single-slider slider-height-1 bg-img" style="background-image:url(/uploads/main_page/slider-1.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-9 col-md-7 col-12 col-sm-12">
                            <div class="slider-content slider-animated-1 pt-230">
                                <h1 class="animated">Kelajakni siz yaratasiz</h1>
                                <p class="animated">Hozirooq bizga qo'shiling! Saytimizda ro'yxatdan o'ting! Online darslarimizda qatnashing va Bilimingizni sinab ko'ring </p>
                                <div class="slider-btn">
                                    <a class="animated default-btn btn-green-color" href="<?= Url::to(['/site/about'])?>">Biz haqimizda</a>
                                    <a class="animated default-btn btn-white-color" href="<?= Url::to(['/site/contact'])?>">Aloqa</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-single-img slider-animated-1">
                        <img class="animated" src="/uploads/main_page/single-slide-1.png" alt="">
                    </div>
                </div>
            </div></div>
        <div class="owl-item cloned" style="width: 1519px;"><div class="single-slider slider-height-1 bg-img" style="background-image:url(/uploads/main_page/slider-1.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-9 col-md-7 col-12 col-sm-12">
                            <div class="slider-content slider-animated-1 pt-230">
                                <h1 class="animated">Kelajakni siz yaratasiz</h1>
                                <p class="animated">Hozirooq bizga qo'shiling! Saytimizda ro'yxatdan o'ting! Online darslarimizda qatnashing va Bilimingizni sinab ko'ring </p>
                                <div class="slider-btn">
                                    <a class="animated default-btn btn-green-color" href="<?= Url::to(['/site/about'])?>">Biz haqimizda</a>
                                    <a class="animated default-btn btn-white-color" href="<?= Url::to(['/site/contact'])?>">Aloqa</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="slider-single-img slider-animated-1">
                        <img class="animated" src="/uploads/main_page/single-slide-1.png" alt="">
                    </div>
                </div>
            </div></div>

    </div>
</div>

<?php
?>

<div class="about-us pt-130 pb-130">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="about-content">
                    <div class="section-title section-title-green mb-30">
                        <h2>Biz <span>haqimizda</span></h2>
                        <p>Bizda bilim olish va bu bilimlarni sinab ko'rish imkoniyati mavjud </p>
                    </div>
                    <p>Bizda maqola va video ko'rinishida darslarni ko'rish imkoniyati va bu darslarda olingan bilimalarni sinab ko'rish imkoniyati mavjud!!!</p>
                    <div class="about-btn mt-45">
                        <a class="default-btn" href="<?= \yii\helpers\Url::to(['/site/about'])?>">Biz haqimizda</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="about-img default-overlay">
                    <img src="/uploads/main_page/banner-1.jpg" alt="">
                    <a class="video-btn video-popup" href="https://www.youtube.com/watch?v=sv5hK4crIRc">
                        <img class="animated" src="/uploads/main_page/video.png" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


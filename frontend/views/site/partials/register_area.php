<?php
?>

<div class="register-area bg-img pt-130 pb-130" style="background-image:url(/uploads/main_page/bg-2.jpg);">
    <div class="container">
        <div class="section-title-2 mb-75 white-text">
            <h2>Hoziroq <span>Ro'yxatdan o'ting</span></h2>
            <p>Bilimingizni sinab ko'ring</p>
        </div>
        <div class="register-wrap">
            <div id="register-3" class="mouse-bg" style="inset: -6.25485% -0.967413%; z-index: 1;">
                <div class="winter-banner">
                    <img src="/uploads/main_page/regi-1.png" alt="">
                    <div class="winter-content">
                        <span>Bilimingni </span>
                        <h3>Sinab</h3>
                        <span>Ko'r </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-md-8">
                    <div class="register-form">
                        <h4>Hoziroq sinab ko'ring</h4>
                        <h3>Foydalanuvchi nomi va parolingizni unutmang!!!</h3>
                        <?php
                            $form = \yii\widgets\ActiveForm::begin();
                        ?>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="contact-form-style mb-20">
                                        <?= $form->field($model, 'username')->textInput(['placeholder'=> 'Foydalanuvchi nomi'])->label(false)?>
                                    </div>
                                </div>
                                <!--<div class="col-lg-6">
                                    <div class="contact-form-style mb-20">
                                        <?/*= $form->field($model, 'email')->textInput(['placeholder'=> 'Email'])->label(false)*/?>
                                    </div>
                                </div>-->
                                <div class="col-lg-6">
                                    <div class="contact-form-style mb-20">
                                        <?= $form->field($model, 'phone')->textInput(['placeholder'=> 'Telefon raqamingiz'])->label(false)?>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="contact-form-style mb-20">
                                        <?= $form->field($model, 'password')->textInput(['placeholder'=> 'Parolingiz'])->label(false)?>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="contact-form-style">
                                        <?= $form->field($model, 'address')->textarea(['placeholder'=> 'Manzilingiz'])->label(false)?>
                                        <button class="submit default-btn text-uppercase" type="submit">Hoziroq qo'shilish</button>
                                    </div>
                                </div>
                            </div>
                        <?php
                            \yii\widgets\ActiveForm::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="register-1" class="mouse-bg" style="inset: -12.5097% -1.93483%; z-index: 1;"></div>
    <div id="register-2" class="mouse-bg" style="inset: -12.5097% -1.93483%; z-index: 1;"></div>
</div>


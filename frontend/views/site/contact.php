<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;


?>

<div class="register-area bg-img pt-100 pb-130" style="background-image:url(/uploads/main_page/bg-2.jpg);">
    <div class="container">
        <div class="section-title-2 mb-75 white-text">
            <h2>Biz bilan <span>Aloqaga chiqing</span></h2>
            <p>Qiziqtirhan savollaringizni yozib qoldiring biz siz bilan bog'lanamiz</p>
        </div>
        <div class="register-wrap">
            <div id="register-3" class="mouse-bg" style="inset: -6.25485% -0.967413%; z-index: 1;">
                <div class="winter-banner">
                    <img src="/uploads/main_page/regi-1.png" alt="">
                    <div class="winter-content">
                        <span>Bilimingni </span>
                        <h3>Sinab</h3>
                        <span>Ko'r </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 col-md-8">
                    <div class="register-form">
                        <h4>Biz bilan bo'g'lanish</h4>
                        <?php
                        $form = \yii\widgets\ActiveForm::begin();
                        ?>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="contact-form-style mb-20">
                                    <?= $form->field($model, 'name')->textInput(['placeholder'=> 'Ismingiz'])->label(false)?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="contact-form-style mb-20">
                                    <?= $form->field($model, 'email')->textInput(['placeholder'=> 'Emailingiz'])->label(false)?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="contact-form-style mb-20">
                                    <?= $form->field($model, 'subject')->textInput(['placeholder'=> 'Mavzu'])->label(false)?>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="contact-form-style mb-20">
                                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                                    ])->label(false) ?>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="contact-form-style">
                                    <?= $form->field($model, 'body')->textarea(['placeholder'=> 'Asosiy qism'])->label(false)?>
                                    <button class="submit default-btn text-uppercase" type="submit">Yuborish</button>
                                </div>
                            </div>
                        </div>
                        <?php
                        \yii\widgets\ActiveForm::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="register-1" class="mouse-bg" style="inset: -12.5097% -1.93483%; z-index: 1;"></div>
    <div id="register-2" class="mouse-bg" style="inset: -12.5097% -1.93483%; z-index: 1;"></div>
</div>

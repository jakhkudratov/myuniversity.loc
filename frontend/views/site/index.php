<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;

/* @var $this yii\web\View */

$this->title = 'My university';

?>
<?=$this->render('partials/top_slider')?>
<?=$this->render('partials/choose_us')?>
<?=$this->render('partials/about_us')?>
<?= /** @var array $sciences */
$this->render('partials/course', [
        'sciences' => $sciences
])?>

<?php
    if (Yii::$app->user->isGuest){
        echo $this->render('partials/register_area', ['model'=> $model]);
    }
?>

<?=$this->render('partials/teachers')?>
<?= /** @var array $articles */
$this->render('partials/event', [
        'articles' => $articles
])?>
<?=$this->render('partials/blog')?>



<?php

use yii\helpers\Url;

$my_sciences = \backend\models\Sciences::find()->limit(7)->all();

?>

<footer class="footer-area">
    <div class="footer-top bg-img default-overlay pt-130 pb-80" style="background-image:url(/uploads/main_page/bg-4.jpg);">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-widget mb-40">
                        <div class="footer-title">
                            <h4>Biz Haqimizda</h4>
                        </div>
                        <div class="footer-about">
                            <p>Bilimli yoshlarga bilimlarini sinab ko'rish imkonini taqdim etamiz</p>
                            <div class="f-contact-info">
                                <div class="single-f-contact-info">
                                    <i class="fa fa-home"></i>
                                    <span>Tashkent, Uzbekistan</span>
                                </div>
                                <div class="single-f-contact-info">
                                    <i class="fa fa-envelope-o"></i>
                                    <span><a href="/">bisiku@gamil.com</a></span>
                                </div>
                                <div class="single-f-contact-info">
                                    <i class="fa fa-phone"></i>
                                    <span> +99890 942 70 76</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="footer-widget mb-40">
                        <div class="footer-title">
                            <h4>Havolalar</h4>
                        </div>
                        <div class="footer-list">
                            <ul>
                                <li><a href="/">Asosiy</a></li>
                                <li><a href="<?= Url::to(['/site/about'])?>">Biz haqimizda</a></li>
                                <li><a href="<?= Url::to(['/science/index'])?>">Kurslarimiz</a></li>
                                <li><a href="<?= Url::to(['/question/index'])?>">Online test</a></li>
                                <li><a href="<?= Url::to(['/question/index'])?>">Test natijalari</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="footer-widget negative-mrg-30 mb-40">
                        <div class="footer-title">
                            <h4>Kurslarimiz</h4>
                        </div>
                        <div class="footer-list">
                            <ul>
                                <?php
                                /** @var \backend\models\Sciences $science */
                                foreach ($my_sciences as $my_science)
                                {
                                ?>
                                <li><a href="<?= Url::to(['/science/view', 'id' => $my_science->id ])?>"><?= $my_science->name?></a></li>
                                <?
                                    }
                                ?>
                                <!--<li><a href="/">Under Graduate Programmes </a></li>
                                <li><a href="/">Graduate Programmes </a></li>
                                <li><a href="/">Diploma Courses</a></li>
                                <li><a href="/">Others Programmes</a></li>
                                <li><a href="/">Short Courses</a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="footer-widget mb-40">
                        <div class="footer-title">
                            <h4>GALLERY</h4>
                        </div>
                        <div class="footer-gallery">
                            <ul class="d-flex">
                                <li><a href="/"><img src="/uploads/main_page/gallery-1.png" alt=""></a></li>
                                <li><a href="/"><img src="/uploads/main_page/gallery-2.png" alt=""></a></li>
                                <li><a href="/"><img src="/uploads/main_page/gallery-3.png" alt=""></a></li>
                                <li><a href="/"><img src="/uploads/main_page/gallery-4.png" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="footer-widget mb-40">
                        <div class="footer-title">
                            <h4>Biz bilan aloqaga chiqish</h4>
                        </div>
                        <div class="subscribe-style">
                            <p>Dugiat nulla pariatur. Edeserunt mollit anim id est laborum. Sed ut perspiciatis unde</p>
                            <div id="mc_embed_signup" class="subscribe-form">
                                <form id="mc-embedded-subscribe-form" class="validate" novalidate="" target="_blank" name="mc-embedded-subscribe-form" method="post" action="#">
                                    <div id="mc_embed_signup_scroll" class="mc-form">
                                        <input class="email" type="email" required="" placeholder="Your E-mail Address" name="EMAIL" value="">
                                        <div class="mc-news" aria-hidden="true">
                                            <input type="text" value="" tabindex="-1" name="b_6bbb9b6f5827bd842d9640c82_05d85f18ef">
                                        </div>
                                        <div class="clear">
                                            <input id="mc-embedded-subscribe" class="button" type="submit" name="subscribe" value="SUBMIT">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom pt-15 pb-15">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 col-md-12">
                    <div class="copyright">
                        <p>
                            Copyright ©
                            <a href="">GBG</a>
                            . All Right Reserved.
                        </p>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12">
                    <div class="footer-menu-social">
                        <div class="footer-menu">
                            <ul>
                                <li><a href="/">Privecy &amp; Policy</a></li>
                                <li><a href="/">Terms &amp; Conditions of Use</a></li>
                            </ul>
                        </div>
                        <div class="footer-social">
                            <ul>
                                <li><a class="facebook" href="/"><i class="fab fa-facebook"></i></a></li>
                                <li><a class="youtube" href="/"><i class="fab fa-youtube"></i></a></li>
                                <li><a class="twitter" href="/"><i class="fab fa-twitter"></i></a></li>
                                <li><a class="google-plus" href="/"><i class="fab fa-google-plus"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

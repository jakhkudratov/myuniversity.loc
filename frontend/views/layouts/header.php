<?php
use yii\helpers\Html;
use yii\helpers\Url;


$sciences = \backend\models\Sciences::find()->all();

?>

<header class="header-area">
    <div class="header-top bg-img" style="background-image:url(/uploads/main_page/header-shape.png);">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-7 col-12 col-sm-8">
                    <div class="header-contact">
                        <ul class="layout-ul">
                            <li><i class="fa fa-phone"></i> +99890 922 80 07</li>
                            <li><i class="fa fa-envelope-o"></i><a href="/">bisiku@gmail.com</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 col-md-5 col-12 col-sm-4">
                    <div class="login-register">
                        <ul class="layout-ul">
                            <?


                            if (Yii::$app->user->isGuest){
                                    ?>
                            <li>
                            <a href="<?= \yii\helpers\Url::to(['/site/login'])?>">Saytga kirish</a>
                            <!--<a href="http://demo.hasthemes.com/glaxdu-v1/glaxdu/login-register.html">Login</a>-->
                            </li>
                                <li><a href="<?= \yii\helpers\Url::to(['/site/signup'])?>">Ro'yxatdan o'tish</a></li>

                                <?php
                                }
                                else{
                                    ?>
                                <li>
                                    <?= Html::a(
                                        'Chiqish',
                                        ['/site/logout'],
                                        ['data-method' => 'post']
                                    )?>
                                </li>
                                    <li>
                                        <a href="#"><?= Yii::$app->user->identity->username?></a>
                                    </li>
                                    <?php
                                }

                            ?>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-bottom sticky-bar clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-6 col-4">
                    <div class="logo logo-main">
                        <a href="/">
                            <img alt="" src="/uploads/main_page/logo.svg">
                        </a>
                    </div>
                </div>
                <div class="col-lg-10 col-md-6 col-8">
                    <div class="menu-cart-wrap">
                        <div class="main-menu">
                            <nav>
                                <ul class="layout-ul">
                                    <li><a href="/"> Asosiy </a>

                                    </li>
                                    <li><a href="<?= Url::to(['/site/about'])?>"> Biz haqimizda  </a></li>
                                    <li class="mega-menu-position top-hover"><a href="<?= Url::to(['/site/courses'])?>"> Bizning darslarimiz <i class="fa fa-angle-down"></i> </a>
                                        <ul class="mega-menu">
                                            <li>
                                                <ul class="layout-ul">
                                                    <li class="mega-menu-title"><a href="<?= Url::to(['/science/index'])?>">Online repititor</a></li>
                                                    <?php

                                                    /** @var \backend\models\Sciences $science */
                                                    foreach ($sciences as $science)
                                                        {
                                                          ?>
                                                            <li><a href="<?= Url::to(['/science/view', 'id' => $science->id ])?>"><?= $science->name?></a></li>
                                                            <?
                                                        }
                                                    ?>

                                                </ul>
                                            </li>
                                            <li>
                                                <ul class="layout-ul">
                                                    <li class="mega-menu-title"><a href="<?= Url::to(['/question/index'])?>">Online Test</a></li>
                                                    <?php
                                                    foreach ($sciences as $science)
                                                    {
                                                        ?>
                                                        <li><a href="<?= Url::to(['/question/view', 'id' => $science->id ])?>"><?= $science->name?></a></li>
                                                        <?
                                                    }

                                                    ?>
                                                </ul>
                                            </li>
                                            <li>
                                                <ul class="layout-ul">
                                                    <li class="mega-menu-title"><a href="<?= Url::to('/result/index')?>">Test Natijalari</a></li>
                                                    <?php
                                                    foreach ($sciences as $science)
                                                    {
                                                        ?>
                                                        <li><a href="<?= Url::to(['/result/result', 'id' => $science->id ])?>"><?= $science->name?></a></li>
                                                        <?
                                                    }
                                                    ?>
                                                </ul>
                                            </li>
                                            <li>
                                                <ul class="layout-ul">
                                                    <li class="mega-menu-title"><a href="<?= Url::to(['/videos/index'])?>">Video Darslar</a></li>
                                                   <?php
                                                   foreach ($sciences as $science)
                                                   {
                                                       ?>
                                                       <li><a href="<?= Url::to(['/videos/view', 'id' => $science->id ])?>"><?= $science->name?></a></li>
                                                       <?
                                                   }

                                                   ?>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>

                                    <li><a href="<?= Url::to(['/site/faq'])?>"> FAQ</a></li>

                                    <li><a href="<?= Url::to(['/site/contact'])?>"> Bog'lanish </a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="cart-search-wrap">

                            <div class="header-search">
                                <button class="search-toggle">
                                    <i class="fa fa-search"></i>
                                </button>
                                <div class="search-content">
                                    <form action="/">
                                        <input type="text" placeholder="Search">
                                        <button>
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobile-menu-area">
                <div class="mobile-menu">
                    <nav id="mobile-menu-active" style="display: block;">
                        <ul class="menu-overflow layout-ul">
                            <li>
                                <!--<a href="http://demo.hasthemes.com/glaxdu-v1/glaxdu/index.html">Asosiy</a>-->
                                <a href="#">Asosiy</a>
                            </li>
                            <li>
                                <!--<a href="http://demo.hasthemes.com/glaxdu-v1/glaxdu/about-us.html">Biz haqimizda</a>-->
                                <a href="#">Biz haqimizda</a>
                            </li>
                            <li>
                                <!--<a href="http://demo.hasthemes.com/glaxdu-v1/glaxdu/shop.html">Bizdagi imkoniyatlar</a>-->
                                <a href="#">Online darslar</a>
                                <ul class="layout-ul">
                                    <li><a href="<?= Url::to(['/science/index'])?>">Online repitetor</a>
                                        <ul>
                                            <?php

                                            /** @var \backend\models\Sciences $science */
                                            foreach ($sciences as $science)
                                            {
                                                ?>
                                                <li><a href="<?= Url::to(['/science/view', 'id' => $science->id ])?>"><?= $science->name?></a></li>
                                                <?
                                            }
                                            ?>

                                        </ul>
                                    </li>
                                    <li><a href="<?= Url::to(['/question/index'])?>">Onlayn test</a>
                                        <ul>
                                            <?php
                                            foreach ($sciences as $science)
                                            {
                                                ?>
                                                <li><a href="<?= Url::to(['/question/view', 'id' => $science->id ])?>"><?= $science->name?></a></li>
                                                <?
                                            }

                                            ?>
                                        </ul>
                                    </li>
                                    <li><a href="<?= Url::to(['/result/index'])?>">Test natijalari</a>
                                        <ul>
                                            <?php
                                            foreach ($sciences as $science)
                                            {
                                                ?>
                                                <li><a href="<?= Url::to(['/result/result', 'id' => $science->id ])?>"><?= $science->name?></a></li>
                                                <?
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                    <li><a href="<?= Url::to(['/videos/index'])?>">Video darslar</a>
                                        <ul>
                                            <?php
                                            foreach ($sciences as $science)
                                            {
                                                ?>
                                                <li><a href="<?= Url::to(['/videos/view', 'id' => $science->id ])?>"><?= $science->name?></a></li>
                                                <?
                                            }

                                            ?>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="/">FAQ</a>

                            </li>

                            <li><a href="http://demo.hasthemes.com/glaxdu-v1/glaxdu/contact.html">Aloqa</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>


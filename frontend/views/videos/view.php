<?php
/* @var $this yii\web\View */
/** @var \backend\models\Article $model */
/** @var array $videos */
?>

<div class="event-area bg-img default-overlay pt-80 pb-100" style="background-image:url(/uploads/main_page/bg-3.jpg);">

    <div class="container">
        <div class="section-title mb-75">
            <h2><span><?= $model->name?></span></h2>
            <p><?= $model->title?></p>
        </div>
        <div class="row">
            <?php
            /** @var \backend\models\Videos $video */
            foreach ($videos as $video)
                {
                    ?>
                    <div class="col-lg-6 col-md-6 mb-4">
                        <h2><?= $video['title']?></h2>
                        <div class="about-img default-overlay">
                            <img src="<?= $video['photo']?>" alt="">
                            <a class="video-btn video-popup" href="<?= $video['link']?>">
                                <img class="animated" src="/uploads/main_page/video.png" alt="">
                            </a>
                        </div>
                    </div>
            <?php
                }
            ?>

        </div>
    </div>
</div>
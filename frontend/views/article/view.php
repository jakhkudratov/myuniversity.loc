<?php
/* @var $this yii\web\View */
/** @var \backend\models\Article $model */
?>

<div class="event-area bg-img pt-80 pb-100" style="background-image:url(/uploads/main_page/bg-3.jpg);">
    <div class="container">
        <div class="section-title mb-75">
            <h2><span><?= $model->title?></span></h2>
            <p><?= $model->desc?></p>
        </div>
        <div class="row">
            <div>
                <img src="<?= $model->photo?>">
            </div>
            <div class="text-main-color">
                <p><?= $model->main_part?></p>

            </div>
        </div>
    </div>
</div>
<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'fontawesome/css/all.min.css',
        'css/site.css',
        //'css/icons.min.css',
        'css/plugins.css',
        'css/style.css',
        'css/my_own_css.css'
    ];
    public $js = [
       // 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js',
        'js/modernizr-2.8.3.min.js',
        //'https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js',
        'js/popper.min.js',
        'js/plugins.js',
        'js/echarts.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset'

    ];
}

<?php

namespace frontend\controllers;

use backend\models\Article;
use backend\models\Sciences;
use yii\web\NotFoundHttpException;

class ArticleController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $sciences = Sciences::find()->all();

        return $this->render('index', [
            'sciences' => $sciences
        ]);
    }
    public function actionView($id)
    {
        $article = $this->findModel($id);
        $article->views += 1;
        $article->save();
        return $this->render('view', [
            'model' => $article
        ]);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

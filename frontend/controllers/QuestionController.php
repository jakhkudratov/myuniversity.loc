<?php

namespace frontend\controllers;

use backend\models\Answer;
use backend\models\Article;
use backend\models\Attemp;
use backend\models\AttempItems;
use backend\models\reason\Objections;
use backend\models\Sciences;
use backend\models\Test;
use backend\models\TestGroup;
use dee\angular\DeeAngularRestAsset;
use frontend\models\TestForm;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use function GuzzleHttp\Psr7\try_fopen;

class QuestionController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $sciences = Sciences::find()->all();
        return $this->render('index', [
            'sciences' => $sciences
        ]);
    }

    public function actionAttemp($id)
    {
        $test_group = $this->findModel($id);
        $attemp = new Attemp();
        $attemp->test_group_id = $test_group->id;
        $attemp->user_id = Yii::$app->user->identity->id;
        $attemp->session_id = Yii::$app->session->id;
        $attemp->science_id = $test_group->science_id;
        $attemp->status = Attemp::STATUS_START;
        if ($attemp->save())
        {
            return $this->redirect(['run', 'id' => $attemp->id]);
        }

        return $this->goBack();

    }
    public function actionRun($id)
    {
        $attemp = $this->findAttemp($id);
        $model = new TestForm();


        if (Yii::$app->request->isPost)
        {
            $post = Yii::$app->request->post();
            $model->attemp_id = $attemp->id;
            $model->test_group_id = $attemp->test_group_id;
            /*d(count($post['Test']));
            dd(count($tests));*/
            $my_tests = Test::find()
                ->where([
                    'test_group_id' => $attemp->test_group_id
                ])
                ->asArray()
                ->all();
            $model->generateAttempItems($post['Test'], $my_tests);

            if ($model->finishidTest())
            {
                return $this->redirect([
                    'finish',
                    'id' => $attemp->id
                ]);
            }
        }

        $data = [];
        $tests = Test::find()
            ->where([
                'test_group_id' => $attemp->test_group_id
            ])
            ->orderBy(new Expression('rand()'))
            ->asArray()
            ->all();
        foreach ($tests as $key => $value)
        {
            $answers = Answer::find()
                ->where([
                    'test_id' => $value['id']
                ])
                ->orderBy(new Expression('rand()'))
                ->asArray()
                ->all();
            $data[] = [
                'test' => $value,
                'answers' => $answers
            ];
        }


        return $this->render('attemp', [
            'data' => $data,
            'model' => $model,

        ]);
    }

    public function actionAttemp2($id)
    {
        $test_group = $this->findModel($id);
        $attemp = new Attemp();
        $attemp->test_group_id = $test_group->id;
        $attemp->user_id = Yii::$app->user->identity->id;
        $attemp->session_id = Yii::$app->session->id;
        $attemp->science_id = $test_group->science_id;
        if (Yii::$app->request->isPost)
        {
            dd(Yii::$app->request->post());
        }
        $test = Test::find()
            ->where([
                'test_group_id' => $test_group->id
            ])
            ->orderBy(new Expression('rand()'))
            ->one();
        $tests = Test::find()
            //->innerJoin(Answer::tableName(), 'test.id=answer.test_id')
            ->where([
                'test_group_id' => $test_group->id
            ])
            ->orderBy(new Expression('rand()'))
            ->asArray()
            ->all();
        $data = [];

        foreach ($tests as $key => $value)
        {

            $answers = Answer::find()
                ->where([
                    'test_id' => $value['id']
                ])
                ->orderBy(new Expression('rand()'))
                ->asArray()
                ->all();
            $data[] = [
                'test' => $value,
                'answers' => $answers
            ];
        }


        $model = new TestForm();
        //dd($data);
        return $this->render('attemp', [
            'data' => $data,
            'model' => $model,

        ]);

        /*
        if ($attemp->save())
            return $this->redirect([
                'attemp-item',
                'id' => $attemp->id,
                'test_id' => $test->id,
                'test_group_id' => $test_group->id
            ]);
       return $this->goBack();*/
    }

    public function actionAttempItem($id,$test_id, $test_group_id)
    {
        //$this->layout = 'main';
        $attemp = $this->findAttemp($id);
        $test = $this->findTest($test_id);
        $count = $this->countChilds($id);
        /*var_dump($attemp);
        var_dump($test_id);
        die();*/
        $answers = Answer::find()
            ->where([
                'test_id' => $test->id
            ])
            ->orderBy(new Expression('rand()'))
            ->all();
       // var_dump($answers);
        $model = new TestForm();
        $model->raqam = $count + 1;
        if (Yii::$app->request->isPost)
        {
            $model->load(Yii::$app->request->post());

            $model->test_group_id = $test_group_id;
            $model->attemp_id = $id;
            $model->test_id = $test->id;


            if ($model->save())
            {
                $next_id = $model->nextTest();
                if ($next_id === null)
                {
                    if ($model->finishidTest())
                    {
                        //var_dump($attemp);die();
                        return $this->redirect([
                            'finish',
                            'id' => $attemp->id
                        ]);
                    }
                }
                return $this->redirect(['attemp-item', 'id' => $id, 'test_id'=>$next_id, 'test_group_id' => $test_group_id]);
            }

        }


        //var_dump($attemp);
        //die();
        return $this->render('test', [
            'model' => $model,
            'answers' => $answers,
            'test' => $test
        ]);
    }

    public function actionFinish($id)
    {
        $attemp = $this->findFinishedAttemp($id);
        $items = AttempItems::find()
            //->innerJoin(Test::tableName(), 'attemp_items.test_id=test.id')
            //->innerJoin(Answer::tableName(), 'attemp_items.answer_id=answer.id')
            //->select(['test.question', 'test.answer_id', 'attemp_items.answer_id'])
            ->where([
                'attemp_id' => $attemp->id
            ])
            ->asArray()
            ->all();
       //dd($items);
        $data = [];
        foreach ($items as $item)
        {
            $answer = Answer::find()->where(['id'=>$item['answer_id']])->one();
            $test = Test::findOne($item['test_id']);
            $data[] = [
                'question' => $item['question'],
                'answer' => $answer->value,
                'right_answer' => $this->findAnswer($test->answer_id),
                'is_right' => ($answer->id === $test->answer_id)?1:0,
                'test_id' => $item['test_id']
            ];
        }
       /// dd($data);
        $objection = new Objections();
        return $this->render('finish', [
            'model' => $attemp,
            'items' => $data,
            'objection' => $objection
        ]);
    }

    public function actionView($id)
    {
        $science = $this->findScience($id);
        $test_groups = TestGroup::find()
            ->where([
                'science_id' => $science->id
            ])
            ->andWhere([
                'not', ['status' => TestGroup::STATUS_DELETED]
            ])
            ->all();
        return $this->render('view',[
            'test_groups' => $test_groups,
            'model' => $science
        ]);
    }

    public function actionScienseTests($science_id)
    {
        $science = $this->findScience($science_id);
        $test_groups = TestGroup::find()
            ->where([
                'science_id' => $science_id
            ])
            ->asArray()
            ->all();


        return $this->render('science', [
            'science' => $science,
            'test_groups' => $test_groups
        ]);
    }
    public function actionReason()
    {
        if (Yii::$app->request->isAjax)
        {

            $post = Yii::$app->request->post();
            //return $post;
            $model = new Objections();
            $model->test_id = (int)$post['test_id'];
            $model->reason_id = (int)$post['reason_id'];
            $model->comment = $post['comment'];
            if ($model->save())
                return true;
            else
                return $model->errors;
            return false;
        }
        return false;
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TestGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TestGroup::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findAttemp($id)
    {
        if (($model = Attemp::find()->where(['id' => $id])->andWhere(['status' => Attemp::STATUS_START])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function findFinishedAttemp($id)
    {
        if (($model = Attemp::find()->where(['id' => $id])->andWhere(['status' => Attemp::STATUS_FINISH])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function countChilds($id)
    {
        $childs = AttempItems::find()->where(['attemp_id' => $id])->count();
        return $childs;
    }
    protected function findTest($id)
    {
        if (($model = Test::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function findScience($id)
    {
        if (($model = Sciences::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function findAnswer($id)
    {
        if (($model = Answer::findOne($id)) !== null) {
            return $model->value;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}

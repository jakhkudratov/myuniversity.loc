<?php

namespace frontend\controllers;

use backend\models\Article;
use backend\models\Sciences;
use dee\angular\DeeAngularRestAsset;
use yii\web\NotFoundHttpException;

class ScienceController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $sciences = Sciences::find()->all();
        return $this->render('index', [
            'sciences' => $sciences
        ]);
    }
    public function actionView($id)
    {
        $science = $this->findModel($id);
        $articles = Article::find()
            ->where([
                'science_id' => $id
            ])->all();

        return $this->render('view', [
            'model' => $science,
            'articles' => $articles
        ]);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sciences the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sciences::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

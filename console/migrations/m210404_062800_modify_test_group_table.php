<?php

use yii\db\Migration;

/**
 * Class m210404_062800_modify_test_group_table
 */
class m210404_062800_modify_test_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\TestGroup::tableName(), 'created_at', $this->integer());
        $this->addColumn(\backend\models\TestGroup::tableName(), 'updated_at', $this->integer());
        $this->addColumn(\backend\models\TestGroup::tableName(), 'created_by', $this->integer());
        $this->addColumn(\backend\models\TestGroup::tableName(), 'updated_by', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210404_062800_modify_test_group_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210404_062800_modify_test_group_table cannot be reverted.\n";

        return false;
    }
    */
}

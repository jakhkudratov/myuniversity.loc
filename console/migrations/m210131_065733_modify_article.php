<?php

use yii\db\Migration;

/**
 * Class m210131_065733_modify_article
 */
class m210131_065733_modify_article extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\Article::tableName(), 'photo', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210131_065733_modify_article cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210131_065733_modify_article cannot be reverted.\n";

        return false;
    }
    */
}

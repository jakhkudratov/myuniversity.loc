<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sciences}}`.
 */
class m210124_103631_create_sciences_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sciences}}', [
            'id' => $this->primaryKey(),
            'photo' => $this->string(),
            'name' => $this->string(),
            'title' => $this->string(),
            'slug' => $this->string()->unique()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sciences}}');
    }
}

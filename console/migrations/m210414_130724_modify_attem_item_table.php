<?php

use yii\db\Migration;

/**
 * Class m210414_130724_modify_attem_item_table
 */
class m210414_130724_modify_attem_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\AttempItems::tableName(), 'status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210414_130724_modify_attem_item_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210414_130724_modify_attem_item_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m210507_070858_modify_all_results
 */
class m210507_070858_modify_all_results extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\result\AllResults::tableName(), 'created_at', $this->integer());
        $this->addColumn(\backend\models\result\AllResults::tableName(), 'updated_at', $this->integer());
        $this->addColumn(\backend\models\result\AllResults::tableName(), 'updated_by', $this->integer());
        $this->addColumn(\backend\models\result\AllResults::tableName(), 'created_by', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210507_070858_modify_all_results cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210507_070858_modify_all_results cannot be reverted.\n";

        return false;
    }
    */
}

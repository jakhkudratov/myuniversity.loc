<?php

use yii\db\Migration;

/**
 * Class m210404_064625_modify_attemp_table
 */
class m210404_064625_modify_attemp_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\Attemp::tableName(), 'status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210404_064625_modify_attemp_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210404_064625_modify_attemp_table cannot be reverted.\n";

        return false;
    }
    */
}

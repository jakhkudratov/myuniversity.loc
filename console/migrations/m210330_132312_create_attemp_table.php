<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%attemp}}`.
 */
class m210330_132312_create_attemp_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%attemp}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'test_group_id' => $this->integer(),
            'right_answers' => $this->integer(),
            'wrong_answers' => $this->integer()
        ]);

        $this->createTable('{{%attemp_items}}', [
            'id' => $this->primaryKey(),
            'attemp_id' => $this->integer(),
            'test_id' => $this->integer(),
            'answer_id' => $this->integer()
        ]);

        $this->createIndex(
            'idx-attem-user_id',
            'attemp',
            'user_id'
        );
        $this->addForeignKey(
            'frk-attemp-user_id',
            'attemp',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-attem-test_group_id',
            'attemp',
            'test_group_id'
        );
        $this->addForeignKey(
            'frk-attemp-test_group_id',
            'attemp',
            'test_group_id',
            'test_group',
            'id',
            'CASCADE'
        );


        $this->createIndex(
            'idx-attemp_items-attemp_id',
            'attemp_items',
            'attemp_id'
        );


        $this->addForeignKey(
            'frk-attemp_items-attemp_id',
            'attemp_items',
            'attemp_id',
            'attemp',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-attemp_items-test_id',
            'attemp_items',
            'test_id'
        );


        $this->addForeignKey(
            'frk-attemp_items-test_id',
            'attemp_items',
            'test_id',
            'test',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-attemp_items-answer_id',
            'attemp_items',
            'answer_id'
        );


        $this->addForeignKey(
            'frk-attemp_items-answer_id',
            'attemp_items',
            'answer_id',
            'answer',
            'id',
            'CASCADE'
        );




    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%attemp}}');
    }
}

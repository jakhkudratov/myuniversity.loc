<?php

use yii\db\Migration;

/**
 * Class m210131_055652_modify_user
 */
class m210131_055652_modify_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\models\User::tableName(), 'verification_token', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210131_055652_modify_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210131_055652_modify_user cannot be reverted.\n";

        return false;
    }
    */
}

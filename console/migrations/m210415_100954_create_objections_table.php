<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%objections}}`.
 */
class m210415_100954_create_objections_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reason', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'desc' => $this->text()
        ]);

        $this->createTable('{{%objections}}', [
            'id' => $this->primaryKey(),
            'test_id' => $this->integer(),
            'reason_id' => $this->integer(),
            'comment' => $this->text(),
            'test_group_id' => $this->integer()
        ]);

        $this->createIndex(
            'idx-objections-test_id',
            'objections',
            'test_id'
        );

        $this->addForeignKey(
            'fk-objections-test_id',
            'objections',
            'test_id',
            'test',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-objections-reason_id',
            'objections',
            'reason_id'
        );

        $this->addForeignKey(
            'fk-objections-reason_id',
            'objections',
            'reason_id',
            'reason',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-objections-test_group_id',
            'objections',
            'test_group_id'
        );

        $this->addForeignKey(
            'fk-objections-test_group_id',
            'objections',
            'test_group_id',
            'test_group',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%objections}}');
    }
}

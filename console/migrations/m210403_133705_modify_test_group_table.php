<?php

use yii\db\Migration;

/**
 * Class m210403_133705_modify_test_group_table
 */
class m210403_133705_modify_test_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\TestGroup::tableName(), 'attemps', $this->integer());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210403_133705_modify_test_group_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210403_133705_modify_test_group_table cannot be reverted.\n";

        return false;
    }
    */
}

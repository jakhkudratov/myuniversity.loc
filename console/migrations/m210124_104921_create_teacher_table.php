<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%teacher}}`.
 */
class m210124_104921_create_teacher_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%teacher}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'science_id' => $this->integer(),
        ]);
        $this->createIndex(
            'fx-teacher-science_id',
            'teacher',
            'science_id'
        );
        $this->addForeignKey(
            'fbx-teacher-science_id',
            'teacher',
            'science_id',
            'sciences',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%teacher}}');
    }
}

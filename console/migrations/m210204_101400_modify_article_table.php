<?php

use yii\db\Migration;

/**
 * Class m210204_101400_modify_article_table
 */
class m210204_101400_modify_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\Article::tableName(), 'user_id', $this->integer());
        $this->addColumn(\backend\models\Article::tableName(), 'created_at', $this->integer());
        $this->addColumn(\backend\models\Article::tableName(), 'updated_at', $this->integer());

        $this->createIndex(
            'index-article-user_id',
            \backend\models\Article::tableName(),
            'user_id'
        );

        $this->addForeignKey(
            'fk-article-user_id',
            \backend\models\Article::tableName(),
            'user_id',
            \common\models\User::tableName(),
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210204_101400_modify_article_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210204_101400_modify_article_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m210507_064838_all_results_table
 */
class m210507_064838_all_results_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('all_results', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'science_id' => $this->integer(),
            'right_answers' => $this->integer(),
            'wrong_answers' => $this->integer(),
            'result' => $this->integer(),
            'attemps' => $this->integer()

        ]);


        // creates index for column `author_id`
        $this->createIndex(
            'idx-all_results-user_id',
            'all_results',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-all_results-user_id',
            'all_results',
            'user_id',
            \common\models\User::tableName(),
            'id',
            'CASCADE'
        );
        // creates index for column `author_id`
        $this->createIndex(
            'idx-all_results-science_id',
            'all_results',
            'science_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-all_results-science_id',
            'all_results',
            'science_id',
            \backend\models\Sciences::tableName(),
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210507_064838_all_results_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210507_064838_all_results_table cannot be reverted.\n";

        return false;
    }
    */
}

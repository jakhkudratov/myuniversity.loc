<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%test_group}}`.
 */
class m210330_122554_create_test_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%test_group}}', [
            'id' => $this->primaryKey(),
            'science_id' => $this->integer(),
            'title' => $this->string(),

        ]);

        $this->createTable('{{%test}}', [
            'id' => $this->primaryKey(),
            'test_group_id' => $this->integer(),
            'question' => $this->text(),
            'answer_id' => $this->integer()
        ]);

        $this->createTable('{{%answer}}', [
            'id' => $this->primaryKey(),
            'test_id' => $this->integer(),
            'value' => $this->text()
        ]);

        $this->createIndex(
            'idx-test_group-science_id',
            'test_group',
            'science_id'
        );

        $this->addForeignKey(
            'fk-test_group-science_id',
            'test_group',
            'science_id',
            'sciences',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-test-test_group_id',
            'test',
            'test_group_id'
        );

        $this->addForeignKey(
            'fk-test-test_group_id',
            'test',
            'test_group_id',
            'test_group',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-answer-test_id',
            'answer',
            'test_id'
        );

        $this->addForeignKey(
            'fk-answer-test_id',
            'answer',
            'test_id',
            'test',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-test-answer_id',
            'test',
            'answer_id'
        );

        $this->addForeignKey(
            'fk-test-answer_id',
            'test',
            'answer_id',
            'answer',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%test_group}}');
    }
}

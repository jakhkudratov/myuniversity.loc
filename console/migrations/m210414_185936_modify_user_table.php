<?php

use yii\db\Migration;

/**
 * Class m210414_185936_modify_user_table
 */
class m210414_185936_modify_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\common\models\User::tableName(), 'phone', $this->string());
        $this->addColumn(\common\models\User::tableName(), 'address', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210414_185936_modify_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210414_185936_modify_user_table cannot be reverted.\n";

        return false;
    }
    */
}

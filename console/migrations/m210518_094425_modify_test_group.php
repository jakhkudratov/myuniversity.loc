<?php

use yii\db\Migration;

/**
 * Class m210518_094425_modify_test_group
 */
class m210518_094425_modify_test_group extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\TestGroup::tableName(), 'status', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210518_094425_modify_test_group cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210518_094425_modify_test_group cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m210409_183356_modify_attemp_table
 */
class m210409_183356_modify_attemp_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\Attemp::tableName(), 'science_id', $this->integer());
        $this->createIndex(
            'idx-attemp-science_id',
            'attemp',
            'science_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-attemp-science_id',
            'attemp',
            'science_id',
            'sciences',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210409_183356_modify_attemp_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210409_183356_modify_attemp_table cannot be reverted.\n";

        return false;
    }
    */
}

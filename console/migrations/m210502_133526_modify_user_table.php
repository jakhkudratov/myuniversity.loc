<?php

use yii\db\Migration;

/**
 * Class m210502_133526_modify_user_table
 */
class m210502_133526_modify_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(\common\models\User::tableName(), 'email', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210502_133526_modify_user_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210502_133526_modify_user_table cannot be reverted.\n";

        return false;
    }
    */
}

<?php

use yii\db\Migration;

/**
 * Class m210204_105240_modify_article_table_add
 */
class m210204_105240_modify_article_table_add extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\Article::tableName(), 'views', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210204_105240_modify_article_table_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210204_105240_modify_article_table_add cannot be reverted.\n";

        return false;
    }
    */
}

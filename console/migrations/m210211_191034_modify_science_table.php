<?php

use yii\db\Migration;

/**
 * Class m210211_191034_modify_science_table
 */
class m210211_191034_modify_science_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\Sciences::tableName(), 'created_at', $this->integer());
        $this->addColumn(\backend\models\Sciences::tableName(), 'updated_at', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210211_191034_modify_science_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210211_191034_modify_science_table cannot be reverted.\n";

        return false;
    }
    */
}

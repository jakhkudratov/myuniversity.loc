<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%article}}`.
 */
class m210124_104327_create_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'desc' => $this->string(),
            'science_id' => $this->integer(),
            'main_part' => $this->text()
        ]);
        $this->createIndex(
            'fx-article-science_id',
            'article',
            'science_id'
        );
        $this->addForeignKey(
            'fbx-article-science_id',
            'article',
            'science_id',
            'sciences',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%article}}');
    }
}

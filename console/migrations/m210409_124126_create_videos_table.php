<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%videos}}`.
 */
class m210409_124126_create_videos_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%videos}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'desc' => $this->string(),
            'science_id' => $this->integer(),
            'link' => $this->text()
        ]);

        $this->createIndex(
            'fx-videos-science_id',
            'videos',
            'science_id'
        );
        $this->addForeignKey(
            'fbx-videos-science_id',
            'videos',
            'science_id',
            'sciences',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%videos}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m210507_072848_modify_all_results
 */
class m210507_072848_modify_all_results extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(\backend\models\result\AllResults::tableName(), 'result', $this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210507_072848_modify_all_results cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210507_072848_modify_all_results cannot be reverted.\n";

        return false;
    }
    */
}

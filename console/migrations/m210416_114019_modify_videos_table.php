<?php

use yii\db\Migration;

/**
 * Class m210416_114019_modify_videos_table
 */
class m210416_114019_modify_videos_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\backend\models\Videos::tableName(), 'photo', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210416_114019_modify_videos_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210416_114019_modify_videos_table cannot be reverted.\n";

        return false;
    }
    */
}

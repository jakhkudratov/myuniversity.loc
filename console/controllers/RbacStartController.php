<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * 
 */
class RbacStartController extends Controller
{
	
	public function actionInit()
	{

		$auth = Yii::$app->authManager;

		/*$student = $auth->createRole('student');
		$auth->add($user);
*/
		$student = $auth->createRole('student');
		$auth->add($student);
		
		$teacher = $auth->createRole('teacher');
		$auth->add($teacher);

		$auth->addChild($teacher, $student);	
			 
	}
}